package com.updater;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
//import java.net.ServerSocket;
import java.net.Socket;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

public class ServerConnectorService extends Service implements Runnable{
	private ServerSocket server;
	private Socket sock;
	private String myName;
	private String serverIp;
	private int port;
	private UpdaterSettingActivity activity;
	private volatile boolean waitForPackage = true;
	public String status;
	private volatile boolean connected = false;
	
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Thread serverMonitorThread = new Thread(this);
		serverMonitorThread.start();
	
		return super.onStartCommand(intent, flags, startId);
	}

	public ServerConnectorService(String name,String ip, int port_, UpdaterSettingActivity activity_){
		myName = name;
		serverIp = ip;
		port = port_;
		activity = activity_;
		try {
			server = new ServerSocket(port);
			connected = true;
		} catch (IOException e) {
			status =  "Failed to gain access to port " + port +": " + e.getMessage();
			waitForPackage = false;
		}
	}
	
	public boolean isConnected(){
		return connected;
	}
	
	@Override
	public void run() {
		try{
			sock = new Socket(serverIp, port);
			PrintWriter pw = new PrintWriter(sock.getOutputStream());
			pw.println(myName);
			pw.flush();
			sock.close();
			System.out.println("Connected");
			toast("Connected to " + serverIp);
			while(waitForPackage){
				try{
					receivePackage();
				}catch(Exception e){
					System.out.println("EXCXXXXCEPTION:" + e.getMessage());
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void receivePackage() throws IOException{
		sock = server.accept();
		System.out.println("-------Receiving File---------");
		FileOutputStream fileStream = activity.getFileStream();
		if(fileStream == null){
			sock.close();
			return;
		}
		int maxPacketSize = 200000;
		int bytesRead = 0;
		try {
			InputStream iStream = sock.getInputStream();
			byte[] buffer = new byte[maxPacketSize];
//			isReceivingFile = true;
			while(bytesRead > -1){
				bytesRead = iStream.read(buffer);
				if(bytesRead>0) fileStream.write(buffer, 0, bytesRead);
			}
			fileStream.flush();
//			isReceivingFile = false;
		} catch (Exception e) {
		}
		sock.close();
		System.out.println("-------Transfer Ended---------");
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	protected void toast(String msg){
//		Looper.prepare();
		Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
	}
}
