package com.updater;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;

public class UpdaterSettingActivity extends Activity {	
	private TextView txtMyName;
	private TextView txtServerIp;
	private TextView txtPort;
	private Button bConnect;
	private Thread serverConnectionThread;
	private ServerConnectorService connector;
	private String filename = "package.apk";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_updater_setting);
		txtMyName = (TextView)findViewById(R.id.txtMyName);
		txtServerIp = (TextView)findViewById(R.id.txtServerIp);
		txtPort = (TextView)findViewById(R.id.txtPort);
		bConnect = (Button)findViewById(R.id.bConnect);
		toast(Environment.getExternalStorageDirectory().toString());
		toast(getFilesDir().toString());
	}

	public void onClick_Connect(View v){
		try{
			setControlsEnabledState(false);
			int port = Integer.parseInt(txtPort.getText().toString());
			connector = new ServerConnectorService(txtMyName.getText().toString(), txtServerIp.getText().toString(), port, this);
			if(connector.isConnected()){
				startService(new Intent(this,ServerConnectorService.class));
				finish();
			}else{
				toast(connector.status);
			}
		}catch(Exception e){
			setControlsEnabledState(true);
			e.printStackTrace();
			toast(e.getMessage());
		}
	}

	protected FileOutputStream getFileStream(){
		File file = new File(Environment.getExternalStorageDirectory() + 
				File.separator + ".update_service" + File.separator + filename);
		try {
			if(file.exists()){
				if(!file.delete()){
					return null;
				}
			}
			file.getParentFile().mkdirs();
			file.createNewFile();
			return new FileOutputStream(file);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	protected void installPackage() {
		String filepath = Environment.getExternalStorageDirectory() + 
				File.separator + ".update_service" + File.separator + filename;
		File packageFile = new File(filepath);
		String extension = android.webkit.MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(packageFile).toString());
		String mimetype = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
		Intent packageInstaller = new Intent(Intent.ACTION_VIEW);
		packageInstaller.setDataAndType(Uri.fromFile(packageFile), mimetype);
		startActivityForResult(packageInstaller,0);
		
	}
	
	private void setControlsEnabledState(boolean enabled){
		txtMyName.setEnabled(enabled);
		txtPort.setEnabled(enabled);
		txtServerIp.setEnabled(enabled);
		bConnect.setEnabled(enabled);
	}
	
	protected void toast(String msg){
//		Looper.prepare();
		Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
	}
	
}
