package com.example.testnetworkcommunication;
import java.io.*;
import java.net.*;

public class Channel {
	private ServerSocket serv;
	private Socket sock;
	private BufferedReader recv;
	private PrintWriter snd;
	private volatile FileOutputStream fileStream;
	private volatile boolean isReceivingFile = false;
	public volatile int receivedBytes = 0;
	
	public boolean sendFile(File file){
		int packetSize = 150000;
		int bytesRead = 0;
		try {
			OutputStream oStream = sock.getOutputStream();
//			oStream.writeObject("SENDING_FILE|" + file.getName() + "|" + fileSize);
			
			FileInputStream fileStream = new FileInputStream(file);
			byte[] buffer = new byte[packetSize];
			System.out.println("Sending File...");
			while(bytesRead > -1){
				bytesRead = fileStream.read(buffer);
				if(bytesRead>0)oStream.write(buffer,0,bytesRead);
			}
//			oStream.writeObject("SEND_COMPLETE");
			oStream.flush();
			System.out.println("Transfer Complete");
		} catch (Exception e) {
			System.out.println("bytesRead = "+bytesRead);
			System.out.println("Exception caught with message:");
			System.out.println(e.getMessage());
			return false;
		} 
		return true;
	}
	
	public void receiveFile(final FileOutputStream _fileStream){
		Thread receiverThread = new Thread(new Runnable() {
			public void run() {
				fileStream = _fileStream;
				if(fileStream == null){return;}
				int maxPacketSize = 200000;
				int bytesRead = 0;
				receivedBytes = 0;
				try {
					InputStream iStream = sock.getInputStream();
					byte[] buffer = new byte[maxPacketSize];
					isReceivingFile = true;
					System.out.println("Receiving File...");
					while(bytesRead > -1){
						bytesRead = iStream.read(buffer);
						if(bytesRead>0) fileStream.write(buffer, 0, bytesRead);
						receivedBytes += bytesRead;
					}
					fileStream.flush();
					isReceivingFile = false;
					System.out.println("Transfer Complete");
				} catch (Exception e) {
					System.out.println("Exception caught with message:");
					System.out.println(e.getMessage());
				}
			}
		});
		receiverThread.start();
		isReceivingFile = true;
	}

	public boolean transferComplete(){
		return !isReceivingFile;
	}
	
	Channel(int port) throws UnknownHostException, IOException{
		serv = null;
		sock = new Socket("localhost", port);
		snd = new PrintWriter(sock.getOutputStream(), true);
		recv = new BufferedReader(new InputStreamReader(sock.getInputStream()));
	}

	Channel(String host, int port) throws UnknownHostException, IOException{
		serv = null;
		sock = new Socket(host, port);
		snd = new PrintWriter(sock.getOutputStream(), true);
		recv = new BufferedReader(new InputStreamReader(sock.getInputStream()));
	}

	Channel(ServerSocket s) throws IOException{
		serv = s;
		sock = s.accept();
		snd = new PrintWriter(sock.getOutputStream(), true);
		recv = new BufferedReader(new InputStreamReader(sock.getInputStream()));
	}

	public String receive() throws IOException{
		return recv.readLine();
	}

	public void send(String b){
		snd.println(b);
	}

	public boolean ready() throws IOException{
		return recv.ready();
	}

	public void close() throws IOException{
		snd.close();
		recv.close();
		sock.close();
		if (serv != null) {
			serv.close();
		}
	}

}
