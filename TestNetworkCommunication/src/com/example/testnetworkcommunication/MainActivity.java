package com.example.testnetworkcommunication;

import java.io.File;

import android.net.Uri;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	Channel ch;
	File packageFile;
	@SuppressLint({ "WorldReadableFiles", "WorldWriteableFiles" })
	public void btnConnect_onClick(View v){
		connectAndTransfer(((EditText)findViewById(R.id.txtServer)).getText().toString());
	}
	
	private void connectAndTransfer(final String serverAddress){
		((EditText)findViewById(R.id.txtServer)).setEnabled(false);
		((Button)findViewById(R.id.btnReceive)).setEnabled(false);
		Thread statusThread = new Thread(new Runnable() {
			public void run() {
				try {
					status("Connecting...");
					ch = new Channel(serverAddress,8080);
					System.out.println("--PATH--");
					String filename = "package.apk";
					String filepath = getFilesDir().getPath()+File.separator+filename;
					packageFile = new File(filepath);
					ch.receiveFile(openFileOutput(filename, MODE_WORLD_READABLE | MODE_WORLD_WRITEABLE));
					while (!ch.transferComplete()){
						String progress = "0.0";
						String postfix = " bytes";
						if(ch.receivedBytes>1024){
							postfix = " KB";
							progress = String.valueOf(ch.receivedBytes / 1024.0);
							if(ch.receivedBytes>1024*1024){
								progress = String.valueOf(ch.receivedBytes / (1024 * 1024.0));
								postfix = " MB";	
							}
						}
						if(progress.contains(".")){
							int dotIndex = progress.indexOf(".");
							progress = progress.substring(0, dotIndex+2);
						}
						status(progress + postfix);
						Thread.sleep(200);
					}
					// Install the package
					String extension = android.webkit.MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(packageFile).toString());
					String mimetype = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
					Intent packageInstaller = new Intent(Intent.ACTION_VIEW);
					packageInstaller.setDataAndType(Uri.fromFile(packageFile), mimetype);
					startActivityForResult(packageInstaller,0);
					Singleton si = Singleton.getInstance();
					synchronized(si){
							si.wait();
					}
					packageFile.delete();
				} catch (Exception e) {
					status("EXCEPTION:" + e.getMessage());
					e.printStackTrace();
				}
				runOnUiThread(new Runnable() {
					public void run() {
						((EditText)findViewById(R.id.txtServer)).setEnabled(true);
						((Button)findViewById(R.id.btnReceive)).setEnabled(true);
					}
				});
			}
		});
		statusThread.start();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Singleton si = Singleton.getInstance();
		synchronized(si){
          si.notify();
        }
		super.onActivityResult(requestCode, resultCode, data);
	}

	private synchronized void status(Object o){
		status(o.toString());
	}
	
	private synchronized void status(final String message){
		runOnUiThread(new Runnable() {
			public void run() {
				System.out.println("STATUS: " + message);
				TextView txtStatus = (TextView)findViewById(R.id.txtStatus);
				txtStatus.setText(message);
			}
		});
	}	
	
	
}
