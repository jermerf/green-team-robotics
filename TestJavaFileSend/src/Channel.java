import java.io.*;
import java.net.*;

import javax.swing.JFileChooser;

public class Channel {
	private ServerSocket serv;
	private Socket sock;
	private BufferedReader recv;
	private PrintWriter snd;
	public static void main(String[] args) {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Channel ch;
		int myJob = 0;
		LEVEL_01();
		boolean retryQuestion = true;
		while(retryQuestion){
			retryQuestion = false;
			System.out.println("What do you want me to be? ( 1, 2, 3, 4 )");
			System.out.println("\t(Server) 1 - Message Receiver");
			System.out.println("\t(Client) 2 - Message Sender");
			System.out.println("\t(Server) 3 - File Sender");
			System.out.println("\t(Client) 4 - File Receiver");
			try {
				myJob = Integer.parseInt(br.readLine());
			} catch (Exception e) { }
			if(myJob < 1 || myJob > 4){
				retryQuestion = true;
			}
		}
		switch (myJob) {
		case 1:
			try {
				ch = new Channel(new ServerSocket(6890));
				while(true){
					if(ch.ready()){
						System.out.println(ch.receive());
					}
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {}
				}
			} catch (IOException e) {
				System.out.println("Failed to start server with error:");
				System.out.println(e.getMessage());
			}
			break;
		case 2:
			try {
				ch = new Channel(6890);
				while(true){
					ch.send(br.readLine());
				}
			} catch (IOException e) {
				System.out.println("Failed to connect to server with error:");
				System.out.println(e.getMessage());
			}
			break;
		case 3:
			JFileChooser chooser = new JFileChooser();
			chooser.showOpenDialog(null);
			try {
				Thread.sleep(250);
			} catch (InterruptedException e1) { }
			while(chooser.getSelectedFile()==null){
				try {
					Thread.sleep(250);
				} catch (InterruptedException e) { }
			}
			System.out.print("File: ");
			System.out.println(chooser.getSelectedFile().getPath());
			System.out.println("Waiting for Connection...");	
			try {
				ch = new Channel(new ServerSocket(6890));
				if(ch.sendFile(chooser.getSelectedFile())){
					ch.close();
				}
			} catch (IOException e) {
				System.out.println("Failed to start server with error:");
				System.out.println(e.getMessage());
			}
			break;
		case 4:
			System.out.print("Please enter the server address: ");
			String serverAddress = "";
			try { serverAddress = br.readLine(); } catch (IOException e2) { }
			JFileChooser saver = new JFileChooser();
			saver.showSaveDialog(null);
			try {
				Thread.sleep(250);
			} catch (InterruptedException e1) { }
			while(saver.getSelectedFile()==null){
				try {
					Thread.sleep(250);
				} catch (InterruptedException e) {}
			}
			System.out.print("Receiving file as: ");
			System.out.println(saver.getSelectedFile().getPath());
			try {
				ch = new Channel(serverAddress, 6890);
				ch.receiveFile(saver.getSelectedFile());
			} catch (IOException e) {
				System.out.println("Failed to connect to server with error:");
				System.out.println(e.getMessage());
			}
			break;
		}
		System.out.println("Exiting");
	}
	
	public boolean sendFile(File file){
		int packetSize = 150000;
		int bytesRead = 0;
		long fileSize = file.length();
		long progress = 0;
		try {
			OutputStream oStream = sock.getOutputStream();
//			oStream.writeObject("SENDING_FILE|" + file.getName() + "|" + fileSize);
			System.out.println(sock.getRemoteSocketAddress().toString());
			FileInputStream fileStream = new FileInputStream(file);
			byte[] buffer = new byte[packetSize];
			System.out.println("Sending File...");
			while(bytesRead > -1){
				System.out.print("\b\b\b\b\b" + (int)(100.0*progress/fileSize)+" %");
				bytesRead = fileStream.read(buffer);
				if(bytesRead>0)oStream.write(buffer,0,bytesRead);
				progress += bytesRead;
			}
//			oStream.writeObject("SEND_COMPLETE");
			oStream.flush();
			System.out.println("Transfer Complete");
		} catch (Exception e) {
			System.out.println("bytesRead = "+bytesRead);
			System.out.println("Exception caught with message:");
			System.out.println(e.getMessage());
			return false;
		} 
		return true;
	}
	
	public boolean receiveFile(File file){
		int maxPacketSize = 200000;
		int bytesRead = 0;
		try {
			InputStream iStream = sock.getInputStream();
			FileOutputStream fileStream = new FileOutputStream(file);
			byte[] buffer = new byte[maxPacketSize];
			System.out.println("Receiving File...");
			while(bytesRead > -1){
				bytesRead = iStream.read(buffer);
				if(bytesRead>0) fileStream.write(buffer, 0, bytesRead);
			}
			fileStream.flush();
			System.out.println("Transfer Complete");
		} catch (Exception e) {
			System.out.println("Exception caught with message:");
			System.out.println(e.getMessage());
			return false;
		}
		
		return true;
	}
	
	Channel(int port) throws UnknownHostException, IOException{
		serv = null;
		sock = new Socket("localhost", port);
		snd = new PrintWriter(sock.getOutputStream(), true);
		recv = new BufferedReader(new InputStreamReader(sock.getInputStream()));
	}

	Channel(String host, int port) throws UnknownHostException, IOException{
		serv = null;
		sock = new Socket(host, port);
		snd = new PrintWriter(sock.getOutputStream(), true);
		recv = new BufferedReader(new InputStreamReader(sock.getInputStream()));
	}

	Channel(ServerSocket s) throws IOException{
		serv = s;
		sock = s.accept();
		snd = new PrintWriter(sock.getOutputStream(), true);
		recv = new BufferedReader(new InputStreamReader(sock.getInputStream()));
	}

	public String receive() throws IOException{
		return recv.readLine();
	}

	public void send(String b){
		snd.println(b);
	}

	public boolean ready() throws IOException{
		return recv.ready();
	}

	public void close() throws IOException{
		snd.close();
		recv.close();
		sock.close();
		if (serv != null) {
			serv.close();
		}
	}
	private static void LEVEL_01(){
		LEVEL_02();
	}
	
	private static void LEVEL_02(){
		LEVEL_03();
	}
	private static void LEVEL_03(){
		StackTraceElement[] trace = Thread.currentThread().getStackTrace();
		for(int i = 0; i < trace.length;i++){
			StackTraceElement element = trace[i];
			if(i>0){
				System.out.println("\t"+element.toString());
			}else {
				System.out.println(element.toString());	
			}
		}
		
	}
	

}
