package mun.greenTeam.testMultiView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

/**
 * A dummy fragment representing a section of the app, but that simply
 * displays dummy text.
 */
public class ViewDisplayFragment extends Fragment {
	
	public final static String EXTRA_MESSAGE = "mun.greenTeam.testMultiView.MESSAGE";

	public ViewDisplayFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_view_display,
				container, false);
		return rootView;
	}
	
//	public void sendMessage(View view){
//		Intent intent = new Intent(this, DisplayMessageActivity.class);
//		EditText editText = (EditText) findViewById(R.id.edit_message);
//		String message = editText.getText().toString();
//		intent.putExtra(EXTRA_MESSAGE, message);
//		startActivity(intent);
//	}
}
