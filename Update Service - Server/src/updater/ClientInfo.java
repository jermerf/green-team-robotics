package updater;

import java.net.InetAddress;

public class ClientInfo {
	private String name;
	private InetAddress ip;
	
	public ClientInfo(String name_, InetAddress ip_){
		name = name_;
		ip = ip_;
	}
	
	public String toString(){
		return name + " (" + ip.getHostAddress() + ")";
	}
	
	public void updateName(String newName){
		name = newName;
	}
	
	public InetAddress getIP(){
		return ip;
	}
	
	public boolean isIpSame(ClientInfo otherClient){
		return ip.equals(otherClient.getIP());
	}
}
;