package updater;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.JList;
import javax.swing.border.EtchedBorder;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.awt.Font;

@SuppressWarnings("serial")
public class ServerManagerWindow extends JFrame {

	private JPanel contentPane;
	private JTextField txtPackage;
	private JTextField txtIpAddress;
	private JTextField txtPort;
	private JLabel txtPackageStatus;
	private JLabel txtServerStatus;
	private DefaultListModel<ClientInfo> clients = new DefaultListModel<ClientInfo>();
	private ServerManager server;
	private Thread statusUpdater;
	private JList<ClientInfo> listClients;
	private String packageStatus = "Package not selected";
	
	private volatile boolean sendingPackage = false;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
					ServerManagerWindow frame = new ServerManagerWindow();
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ServerManagerWindow() {
		setTitle("Robot Update Server");
		setResizable(false);
		setType(Type.POPUP);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 505, 303);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblTargetPackage = new JLabel("Target Package");
		lblTargetPackage.setBounds(17, 11, 90, 16);
		
		JButton btnBrowse = new JButton("Browse");
		btnBrowse.setBounds(409, 33, 78, 26);
		
		listClients = new JList<ClientInfo>(clients);
		listClients.setBounds(17, 124, 246, 135);
		listClients.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		
		JButton btnSelectAll = new JButton("Select All");
		btnSelectAll.setBounds(273, 137, 102, 26);
		
		JButton btnSelectNone = new JButton("Select None");
		btnSelectNone.setBounds(385, 137, 102, 26);
		
		JButton btnPushPackageTo = new JButton("Push Package to Selected Devices \u27A3");
		btnPushPackageTo.setBounds(273, 174, 214, 26);
		btnPushPackageTo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnPushPackageTo.setBackground(new Color(144, 238, 144));
		
		JLabel lblServerIp = new JLabel("Server IP");
		lblServerIp.setBounds(17, 72, 52, 16);
		
		txtIpAddress = new JTextField();
		txtIpAddress.setBounds(98, 70, 90, 20);
		txtIpAddress.setEditable(false);
		txtIpAddress.setColumns(10);

		try {
			txtIpAddress.setText(InetAddress.getLocalHost().getHostAddress());
		} catch (Exception e) {
			txtIpAddress.setText(e.getMessage());
		}
		
		JLabel lblListenOnPort = new JLabel("Listen On Port");
		lblListenOnPort.setBounds(17, 99, 81, 16);
		
		txtPort = new JTextField();
		txtPort.setBounds(98, 97, 90, 20);
		txtPort.setText("8080");
		txtPort.setColumns(10);
		
		JButton btnApply = new JButton("Apply");
		btnApply.setBounds(198, 94, 65, 26);
		JScrollPane jp = new JScrollPane(listClients);
		jp.setBounds(17, 139, 246, 120);
		contentPane.setLayout(null);
			
			txtServerStatus = new JLabel("");
			txtServerStatus.setBounds(273, 110, 216, 16);
			contentPane.add(txtServerStatus);
		
			
			txtPackageStatus = new JLabel("");
			txtPackageStatus.setBounds(271, 90, 216, 16);
			contentPane.add(txtPackageStatus);
		
		JLabel lblStatus = new JLabel("Status:");
		lblStatus.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblStatus.setBounds(273, 70, 40, 16);
		contentPane.add(lblStatus);
		
		txtPackage = new JTextField();
		txtPackage.setBounds(17, 36, 378, 20);
		txtPackage.setColumns(10);
		contentPane.add(txtPackage);
		contentPane.add(jp);
		contentPane.add(btnSelectAll);
		contentPane.add(btnSelectNone);
		contentPane.add(lblTargetPackage);
		contentPane.add(lblServerIp);
		contentPane.add(txtIpAddress);
		contentPane.add(lblListenOnPort);
		contentPane.add(txtPort);
		contentPane.add(btnApply);
		contentPane.add(btnBrowse);
		contentPane.add(btnPushPackageTo);
		
		server = new ServerManager(Integer.parseInt(txtPort.getText()),clients);
		
		
		btnApply.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) { onClick_btnApply(); }
		});
		btnBrowse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) { onClick_btnBrowse(); }
		});
		btnSelectAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) { onClick_btnSelectAll(); }
		});
		btnSelectNone.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) { onClick_btnSelectNone(); }
		});
		btnPushPackageTo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) { onClick_btnPushPackageTo(); }
		});
		
		statusUpdater = new Thread(new Runnable() {
			public void run() {
				while(true){
					if(server == null) {
						txtServerStatus.setText("Server Not Running");
						return;
					}
					txtPackageStatus.setText(packageStatus);
					txtServerStatus.setText(server.status);
					if(sendingPackage){
						if(server.status.equals(server.PACKAGES_SENT)){
							sendingPackage = false;
							for(int i=0; i<contentPane.getComponentCount(); i++){
								contentPane.getComponent(i).setEnabled(true);
							}
						}
					}
					try{
						Thread.sleep(1000);
					}catch(Exception e){
						txtServerStatus.setText("StatusUpdater Thread Error");
					}
				}
			}
		});
		statusUpdater.start();
		txtPackage.setText("C:\\acom.gamevil.zenonia.v1.1.1.apk");
	}
	
	private void onClick_btnApply(){
		int newPort;
		try{
			newPort = Integer.parseInt(txtPort.getText());
		}catch(Exception e){
			server.status = "** Invalid Port Number **";
			return;
		}
		server = new ServerManager(newPort, clients);
	}
	
	private void onClick_btnBrowse(){
		
	}
	
	private void onClick_btnSelectAll(){
		int[] allIndices = new int[clients.size()];
		for(int i=0; i<clients.size(); i++){
			allIndices[i] = i;
		}
		listClients.setSelectedIndices(allIndices);
	}
	
	private void onClick_btnSelectNone(){
		listClients.setSelectedIndices(new int[0]);
	}
	
	private void onClick_btnPushPackageTo(){
		File packageFile = new File(txtPackage.getText().trim());
		if(clients.size()==0){
			String msg = "There are no clients. Who will take this";
			if(packageFile.exists()){
				msg += " delicious package?";				
			}else{
				msg += "... are you feeling ok? There's no package either...";
			}
			message(msg);
			return;
		}else if (!packageFile.exists()) {
			message("You haven't selected a package file");
			return;
		}
		for(int i=0; i<contentPane.getComponentCount(); i++){
			contentPane.getComponent(i).setEnabled(false);
		}
		System.out.println("Gonna send from server");
		server.sendPackage(packageFile);
	}
	
	private void message(String msg) {
        JOptionPane.showMessageDialog(null, msg, "Updater Service - Server", JOptionPane.INFORMATION_MESSAGE);
    }
}
