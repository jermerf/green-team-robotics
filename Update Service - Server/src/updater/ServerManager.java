package updater;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

import javax.swing.DefaultListModel;

public class ServerManager implements Runnable{
	private Thread serverThread;
	private int port;
	private ServerSocket serverSocket;
	private volatile boolean serverEnabled = true;
	public String status = "";
	private DefaultListModel<ClientInfo> clients;
	public final String PACKAGES_SENT = "Package transfer complete";
	private File packageFile;
	private boolean sendPackage = false;
	
	public ServerManager(int port_, DefaultListModel<ClientInfo> clients_) {
		port = port_;
		clients = clients_;
		try {
			serverSocket = new ServerSocket(port);
			serverSocket.setSoTimeout(200);
		} catch (Exception e) {
			serverEnabled = false;
			status = e.getMessage();
			return;
		}
		serverThread = new Thread(this);
		serverThread.start();
	}
	
	public void dispose(){
		serverEnabled = false;
		if(serverThread == null) return;
		while(serverThread.isAlive()){
			//Waiting for server to close
		}
	}
	
	public boolean isServerRunning(){
		return serverEnabled;
	}
	
	public void sendPackage(File file){
		packageFile = file;
		sendPackage = true;
	}
	
	public void run() {
		try{
			while(serverEnabled){
				acceptClients();
				if(sendPackage){
					sendPackage();
				}
			}
			serverSocket.close();
		}catch(Exception e){
			serverEnabled = false;
			status = e.getMessage();
		}
	}
	
	private void acceptClients(){
		try{
			Socket s = serverSocket.accept();
			BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
			String nameString = br.readLine();
			boolean inList = false;
			ClientInfo client = new ClientInfo(nameString, s.getInetAddress());
			for(int i=0;i<clients.size();i++){
				if(clients.get(i).isIpSame(client)){
					inList = true;
					clients.setElementAt(client, i);
					break;
				}
			}
			if(!inList){
				clients.addElement(client);
			}
		}catch(SocketTimeoutException se){ 
		}catch (IOException e) {}
	}
	
	private boolean sendPackage(){
		int packetSize = 150000;
		int bytesRead;
		for(int i=0; i<clients.size(); i++){			
			bytesRead = 0;
			status = "Sending to client " + (i + 1) + " of " + clients.size();
			System.out.println(status);
			try{
				ClientInfo client = clients.get(i);
				Socket sock = new Socket(client.getIP(), port);
				OutputStream oStream = sock.getOutputStream();
				FileInputStream fileStream = new FileInputStream(packageFile);
				byte[] buffer = new byte[packetSize];
				while(bytesRead > -1){
					bytesRead = fileStream.read(buffer);
					if(bytesRead>0)oStream.write(buffer,0,bytesRead);
				}
				oStream.flush();
				fileStream.close();
				sock.close();
			}catch(Exception e){
				System.out.println("EXCEPTIONNNNNNNED:\n\t"+e.getMessage());
			}
			
		}
		status = PACKAGES_SENT;
		sendPackage = false;
		return true;
	}
}
