package com.example.testioioopencv;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;

import ioio.lib.util.IOIOLooper;
import ioio.lib.util.android.IOIOActivity;

import com.example.testioioopencv.JavaCameraCustom;

import android.os.Bundle;
import android.view.SurfaceView;
import android.widget.TextView;

public class GuiActivity extends IOIOActivity {
	private static volatile String status = "----";
	private JavaCameraCustom cvRearCamera;
	private CameraListener camListener = new CameraListener();
	private TextView txtStatus;
	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
		public void onManagerConnected(int status) {
			switch (status) {
			case LoaderCallbackInterface.SUCCESS:
				cvRearCamera.enableView();
				break;
			default:
				break;
			}
			super.onManagerConnected(status);
		}
	};
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gui);
		cvRearCamera = (JavaCameraCustom)findViewById(R.id.cvRearCamera);
		cvRearCamera.setVisibility(SurfaceView.VISIBLE);
		cvRearCamera.setCvCameraViewListener(camListener);
		txtStatus = (TextView)findViewById(R.id.txtStatus);
		Thread statusUpdater = new Thread(new Runnable() {
			@Override
			public void run() {
				while(true){
					try {
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								txtStatus.setText(status);
							}
						});
						Thread.sleep(10);
					} catch (Exception e) {
						status = e.getMessage();
					}
				}
			}
		});
		statusUpdater.start();
	}
	
	protected void onResume() {
		super.onResume();
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, mLoaderCallback);
	}

	@Override
	protected IOIOLooper createIOIOLooper() {
		return new IOIOController(camListener);
	}
	
	public static void setStatus(String message){
		status = message;
	}
	
}
