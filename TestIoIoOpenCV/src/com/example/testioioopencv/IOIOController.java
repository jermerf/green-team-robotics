package com.example.testioioopencv;

import ioio.lib.api.PwmOutput;
import ioio.lib.api.exception.ConnectionLostException;
import ioio.lib.util.BaseIOIOLooper;

public class IOIOController extends BaseIOIOLooper{
	private CameraListener cam;
	int pulseTime = 1580;

	public IOIOController(CameraListener camera){
		cam = camera;
	}
	
	int cycles = 0;
	@Override
	public void loop() throws ConnectionLostException, InterruptedException {
		int offset = cam.getOffset();
		ioio_01.setPulseWidth(pulseTime + offset/4);
		GuiActivity.setStatus(cycles++ + ": " + offset);
	}

	PwmOutput ioio_01;
	protected void setup() throws ConnectionLostException, InterruptedException {
		ioio_01 = ioio_.openPwmOutput(1, 400);
	}
	
	@Override
	public void disconnected() {
		super.disconnected();
		GuiActivity.setStatus("IOIO has been disconnected");
	}
}
