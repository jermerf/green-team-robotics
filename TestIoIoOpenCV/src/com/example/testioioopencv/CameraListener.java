package com.example.testioioopencv;

import java.util.ArrayList;
import java.util.List;

import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;

public class CameraListener implements CvCameraViewListener {
	private Scalar targetColourLower = new Scalar(110,100,100,0);
	private Scalar targetColourUpper = new Scalar(210,255,255,255);
	private int contourColor = 100;
	@Override
	public void onCameraViewStarted(int width, int height) { }
	@Override
	public void onCameraViewStopped() { }
	@Override
	public Mat onCameraFrame(Mat ifr) {
		Mat mMask = new Mat();
		Mat mDilatedMask = new Mat();
		Mat ifrHSV = new Mat();
		Imgproc.cvtColor(ifr, ifrHSV, Imgproc.COLOR_RGB2HSV_FULL);
		Core.inRange(ifrHSV, targetColourLower, targetColourUpper, mMask);
		Imgproc.dilate(mMask, mDilatedMask, new Mat());
		List<MatOfPoint> contoursList = new ArrayList<MatOfPoint>();
		Imgproc.findContours(mDilatedMask, contoursList, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
		
		int iContour = -1;
		double mostArea = 0;
		for(int i=0; i< contoursList.size(); i++){
			Mat contour = contoursList.get(i);
			double tmpArea = Imgproc.contourArea(contour);
			if(tmpArea > mostArea){
				mostArea = tmpArea;
				iContour = i;
			}
		}
		contourColor += 50;
		if(contourColor > 250) contourColor = 100;
		if(iContour > -1 && mostArea > 500){
			drawContourCenter(ifr, contoursList.get(iContour));
		}else {
			drawFrame(ifr);
		}
		Imgproc.drawContours(ifr, contoursList, iContour, new Scalar(contourColor,contourColor,contourColor,255));
		mMask.release();
		mDilatedMask.release();
		ifrHSV.release();
		return ifr;
	}
	
	private volatile int y=0;
	private void drawContourCenter(Mat canvas, Mat contour){
		Moments moments = Imgproc.moments(contour);
		int xCenter = (int)(moments.get_m01()/moments.get_m00());
		int yCenter = (int)(moments.get_m10()/moments.get_m00());
		y = canvas.height() - yCenter;
		int xl,xh,yl,yh;
		int pSize = 5;
		xl = xCenter - pSize;
		xh = xCenter + pSize;
		yl = yCenter - pSize;
		yh = yCenter + pSize;
		if(xl < 0) xl = 0;
		if(xh > canvas.rows()) xh = canvas.rows();
		if(yl < 0) yl = 0;
		if(yh > canvas.cols()) yh = canvas.cols();
		Mat drawSquare = canvas.submat(xl, xh, yl, yh);
		drawSquare.setTo(new Scalar(contourColor, contourColor, contourColor, 255));
	}
	
	private void drawFrame(Mat canvas){
		Scalar color = new Scalar(255, 25, 25, 255);
		int thickness = 3;
		Mat tmp = canvas.submat(0, canvas.rows(), 0, thickness);
		tmp.setTo(color);
		tmp = canvas.submat(0, thickness, 0, canvas.cols());
		tmp.setTo(color);
		tmp = canvas.submat(canvas.rows()-thickness, canvas.rows(), 0, canvas.cols());
		tmp.setTo(color);
		tmp = canvas.submat(0, canvas.rows(), canvas.cols()-thickness, canvas.cols());
		tmp.setTo(color);
		tmp.release();
	}
	
	public int getOffset(){
		return -y;
	}

}
