package com.example.testopencv02;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.Toast;
import android.hardware.Camera.Size;
import android.widget.Toast;

public class MainActivity extends Activity implements CvCameraViewListener{
	private final String tag = "--JER--";
	private JavaCameraCustom cvRearCamera;
	private List<Size> resList;
	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
		
		public void onManagerConnected(int status) {
			log("onManagerConnected");
			switch (status) {
			case LoaderCallbackInterface.SUCCESS:
				cvRearCamera.enableView();
				break;
			default:
				break;
			}
			super.onManagerConnected(status);
			log("End onManagerConnected");
		}
	};

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		cvRearCamera = (JavaCameraCustom)findViewById(R.id.cvRearCamera);
		cvRearCamera.setVisibility(SurfaceView.VISIBLE);
		cvRearCamera.setCvCameraViewListener(this);
		cvRearCamera.setSize(720, 480);
	}
	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		SubMenu resMenu = menu.addSubMenu("Choose Resolution");
		SubMenu limitsMenu = menu.addSubMenu("Color Limits");
		resList = cvRearCamera.getResolutionList();
		for(int i=0; i< resList.size(); i++){
			Size s = resList.get(i);
			resMenu.add(1,i, Menu.NONE,s.width + "x" + s.height);
		}
		int index = 0;
		limitsMenu.add(2, index++, Menu.NONE, "Repeat Colors");
		limitsMenu.add(2, index++, Menu.NONE, "Hue+20");
		limitsMenu.add(2, index++, Menu.NONE, "Hue-20");
		limitsMenu.add(2, index++, Menu.NONE, "Hue Range+20");
		limitsMenu.add(2, index++, Menu.NONE, "Hue Range-20");
		limitsMenu.add(2, index++, Menu.NONE, "Satura.Min+20");
		limitsMenu.add(2, index++, Menu.NONE, "Satura.Min-20");
		limitsMenu.add(2, index++, Menu.NONE, "Value Min+20");
		limitsMenu.add(2, index++, Menu.NONE, "Value Min-20");
		return true;
	}

	int hue = 140;
	int hueRange = 100;
	int satMin = 100;
	int valMin = 160;
	public boolean onOptionsItemSelected(MenuItem item){
		log("Menu GroupID: "+ item.getGroupId());
		if(item.getGroupId()==1){
			log("Menu ItemID: "+ item.getItemId());
			log((String)item.getTitle());
			cvRearCamera.setResolution(resList.get(item.getItemId()));
			Size res = cvRearCamera.getResolution();
			Toast.makeText(this, res.width + "x" + res.height, Toast.LENGTH_LONG).show();
		}else if (item.getGroupId()==2) {
			int toastLength = Toast.LENGTH_SHORT;
			switch(item.getItemId()){
			case 0:{
				toastLength = Toast.LENGTH_LONG;
				break;
			}case 1:{
				hue += 20;
				break;
			}case 2:{
				hue -= 20;
				break;
			}case 3:{
				hueRange += 20; 
				break;
			}case 4:{
				hueRange -= 20;
				break;
			}case 5:{
				satMin += 20;
				break;
			}case 6:{
				satMin -= 20;
				break;
			}case 7:{
				valMin += 20;
				break;
			}case 8:{
				valMin -= 20;
				break;
			}}
			if(hue < 0) hue = 0;
			if(hue > 255) hue = 255;
			if(hueRange < 0) hueRange = 0;
			if(hueRange > 255) hueRange = 255;
			int hueMin = hue - hueRange/2;
			int hueMax = hue + hueRange/2;
			if(hueMin < 0) hueMin = 0;
			if(hueMax > 255) hueMax = 255;
			if(satMin < 0) satMin = 0;
			if(satMin > 255) satMin = 255;
			if(valMin < 0) valMin = 0;
			if(valMin > 255) valMin = 255;
			targetColourLower = new Scalar(hueMin, satMin, valMin,255);
			targetColourUpper = new Scalar(hueMax, 255,255,255);
			Toast.makeText(this, "HSV: " + hue + ", " + satMin + " ," + valMin
					+ "\nHueRange: " + hueRange 
					+ "\nHueMin, HueMax: " + hueMin + " ," + hueMax, toastLength).show();
		}
		return true;
	}

	protected void onResume() {
		super.onResume();
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, mLoaderCallback);
	}
	public void onDestroy() {
        super.onDestroy();
        if (cvRearCamera != null)
        	cvRearCamera.disableView();
    }
	
	boolean isFirstFrame = true;
	public void onCameraViewStarted(int width, int height) { 
		if(isFirstFrame){
			isFirstFrame = false;
//			String resolutions = "";
//			List<Size> sizes = cvRearCamera.getResolutions();
//			for(int i=0;i<sizes.size();i++){
//				Size size = sizes.get(i);
//				resolutions += size.width + "x" + size.height + "\n"; 
//			}
//			Toast.makeText(this, resolutions, Toast.LENGTH_LONG).show();
		}
	}
	int contourColor = 100;
	public void onCameraViewStopped() { }

	Scalar targetColourLower = new Scalar(110,100,100,0);
	Scalar targetColourUpper = new Scalar(210,255,255,255);
	@Override
	public Mat onCameraFrame(Mat ifr) {
		Mat mMask = new Mat();
		Mat mDilatedMask = new Mat();
		Mat ifrHSV = new Mat();
		Imgproc.cvtColor(ifr, ifrHSV, Imgproc.COLOR_RGB2HSV_FULL);
		Core.inRange(ifrHSV, targetColourLower, targetColourUpper, mMask);
		Imgproc.dilate(mMask, mDilatedMask, new Mat());
		List<MatOfPoint> contoursList = new ArrayList<MatOfPoint>();
		Imgproc.findContours(mDilatedMask, contoursList, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
		
		int iContour = -1;
		double mostArea = 0;
		for(int i=0; i< contoursList.size(); i++){
			Mat contour = contoursList.get(i);
			double tmpArea = Imgproc.contourArea(contour);
			if(tmpArea > mostArea){
				mostArea = tmpArea;
				iContour = i;
			}
		}
		contourColor += 50;
		if(contourColor > 250) contourColor = 100;
		if(iContour > -1 && mostArea > 500){
			drawContourCenter(ifr, contoursList.get(iContour));
		}else {
			drawFrame(ifr);
		}
		Imgproc.drawContours(ifr, contoursList, iContour, new Scalar(contourColor,contourColor,contourColor,255));
//		Mat pyrDown = ifr.submat(0, mPyrDownMat.rows(), 0, mPyrDownMat.cols());
//		pyrDown.setTo(mPyrDownMat);
		mMask.release();
		mDilatedMask.release();
		ifrHSV.release();
		return ifr;
	}
	
	private void drawContourCenter(Mat canvas, Mat contour){
		Moments moments = Imgproc.moments(contour);
		int xCenter = (int)(moments.get_m01()/moments.get_m00());
		int yCenter = (int)(moments.get_m10()/moments.get_m00());
		int xl,xh,yl,yh;
		int pSize = 5;
		xl = xCenter - pSize;
		xh = xCenter + pSize;
		yl = yCenter - pSize;
		yh = yCenter + pSize;
		if(xl < 0) xl = 0;
		if(xh > canvas.rows()) xh = canvas.rows();
		if(yl < 0) yl = 0;
		if(yh > canvas.cols()) yh = canvas.cols();
		log("ROWSxCOLS:" + canvas.rows() + "x" + canvas.cols());
		log("\tRow Range: " + xl + " -> " + xh);
		log("\tCol Range: " + yl + " -> " + yh);
		Mat drawSquare = canvas.submat(xl, xh, yl, yh);
		drawSquare.setTo(new Scalar(contourColor, contourColor, contourColor, 255));
		log("Area: " + moments.get_m00());
		log("Center: " + xCenter + ", " + yCenter);
//		drawSquare.release();
//		canvas.release();
//		contour.release();
	}
	
	private void drawFrame(Mat canvas){
		Scalar color = new Scalar(255, 25, 25, 255);
		int thickness = 3;
		Mat tmp = canvas.submat(0, canvas.rows(), 0, thickness);
		tmp.setTo(color);
		tmp = canvas.submat(0, thickness, 0, canvas.cols());
		tmp.setTo(color);
		tmp = canvas.submat(canvas.rows()-thickness, canvas.rows(), 0, canvas.cols());
		tmp.setTo(color);
		tmp = canvas.submat(0, canvas.rows(), canvas.cols()-thickness, canvas.cols());
		tmp.setTo(color);
		tmp.release();
	}
	
	private void log(String msg){
		Log.i(tag,msg);
	}
	
	
}