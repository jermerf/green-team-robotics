package com.example.testopencv02;

import java.util.List;

import org.opencv.android.JavaCameraView;
import android.hardware.Camera.Size;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;

public class JavaCameraCustom extends JavaCameraView {
	String tag = "--JER--";
	public JavaCameraCustom(Context context, AttributeSet attrs) {
        super(context, attrs);
	}
	
	public void setRes640x480(){
		disconnectCamera();
		mMaxHeight = 640;
        mMaxWidth = 480;
        connectCamera(getWidth(), getHeight());
	}
	
	public void setResolution(Size res){
		disconnectCamera();
		setSize(res.width, res.height);
		connectCamera(getWidth(), getHeight());
	}
	
	public void setSize(int w, int h){
		mMaxWidth = w;
		mMaxHeight = h;
		
	}
	
	public Size getResolution(){
		return mCamera.getParameters().getPreviewSize();
	}
	
	public List<Size> getResolutionList(){
		if(mCamera==null)Log.i(tag, "NULL mCamera");
		return mCamera.getParameters().getSupportedPreviewSizes();
	}
}