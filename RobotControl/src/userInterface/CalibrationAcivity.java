package userInterface;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.List;

import ioio.lib.util.IOIOLooper;
import ioiohw.*;
import mun.greenTeam.robotcontrol.R;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class CalibrationAcivity extends Activity{
	public static final String defaultFilename = "default_calibration";
	private static IOIOHardware ioio_;
	private CalibratedMotor activeCM;
	private TextView txtForward;
	private TextView txtCentre;
	private TextView txtReverse;
	private TextView txtStatus;
	private boolean isServo = false;
	private Button bCpp;
	private Button bCp;
	private Button bCm;
	private Button bCmm;
	private enum motor {LEFTWHEEL,RIGHTWHEEL,GATE,EJECT};
	private enum dir {FORWARD,CENTRE,REVERSE};
	private motor myMotor=motor.LEFTWHEEL;
	private dir myDir=dir.FORWARD;
	public static int pwm=0;
	public static void setIOIOHardware(IOIOHardware ioio_inst){
		ioio_ = ioio_inst;
	}
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.calibration_layout);
		((RadioButton)findViewById(R.id.radLeftWheel)).setChecked(true);
		txtForward = (TextView)findViewById(R.id.txtForward);
		txtCentre = (TextView)findViewById(R.id.txtCentre);
		txtReverse = (TextView)findViewById(R.id.txtReverse);
		txtStatus = (TextView)findViewById(R.id.txtStatus);
		bCpp = (Button)findViewById(R.id.bCpp);
		bCp = (Button)findViewById(R.id.bCp);
		bCm = (Button)findViewById(R.id.bCm);
		bCmm = (Button)findViewById(R.id.bCmm);
		onClick_radLeftWheel(null);
	}
	
	public void onClick_radLeftWheel(View v){
		activeCM = ioio_.leftWheelHelper;
		myMotor = motor.LEFTWHEEL;
		isServo = false; setupUI();
	}
	public void onClick_radRightWheel(View v){
		activeCM = ioio_.rightWheelHelper;
		myMotor = motor.RIGHTWHEEL;
		isServo = false; setupUI();
	}
	public void onClick_radGateServo(View v){
		activeCM = ioio_.gateServoHelper;
		myMotor = motor.GATE;
		isServo = true; setupUI();
	}
	public void onClick_radEjectServo(View v){
		activeCM = ioio_.ejectServoHelper;
		myMotor = motor.EJECT;
		isServo = true; setupUI();
	}
	public void onClick_bGoBack(View v){ finish(); }
	public void onClick_bFpp(View v){activeCM.fwdMaxPulse +=25; myDir=dir.FORWARD; update();}
	public void onClick_bFp(View v){activeCM.fwdMaxPulse++; myDir=dir.FORWARD; update();}
	public void onClick_bFm(View v){activeCM.fwdMaxPulse--; myDir=dir.FORWARD; update();}
	public void onClick_bFmm(View v){activeCM.fwdMaxPulse -=25; myDir=dir.FORWARD; update();}
	public void onClick_bCpp(View v){activeCM.centrePulse +=25; myDir=dir.CENTRE; update();}
	public void onClick_bCp(View v){activeCM.centrePulse++; myDir=dir.CENTRE; update();}
	public void onClick_bCm(View v){activeCM.centrePulse--; myDir=dir.CENTRE; update();}
	public void onClick_bCmm(View v){activeCM.centrePulse -=25; myDir=dir.CENTRE; update();}
	public void onClick_bRpp(View v){activeCM.revMaxPulse +=25; myDir=dir.REVERSE; update();}
	public void onClick_bRp(View v){activeCM.revMaxPulse++; myDir=dir.REVERSE; update();}
	public void onClick_bRm(View v){activeCM.revMaxPulse--; myDir=dir.REVERSE; update();}
	public void onClick_bRmm(View v){activeCM.revMaxPulse -=25; myDir=dir.REVERSE; update();}
	public void bLoad(View v){ loadDefaults(this);}
	
	//Saving and loading defaults
	//Calibration files are saved as plain text where each line represents one motor's calibration
	// values and the each value is separated by a comma.
	// The values are, from a CalibratedMotor object:
	//
	// 		fwdMaxPulse,revMaxPulse,centrePulse
	//
	// and the motors are the order: left wheel, right wheel, gate servo and eject servo
	public void onClick_bSaveDefault(View v){
		try {	
			OutputStreamWriter os = new OutputStreamWriter(openFileOutput(defaultFilename, MODE_PRIVATE));
			os.write(outCalibration(ioio_.leftWheelHelper) + "\n");
			os.write(outCalibration(ioio_.rightWheelHelper) + "\n");
			os.write(outCalibration(ioio_.gateServoHelper) + "\n");
			os.write(outCalibration(ioio_.ejectServoHelper));
			os.flush();
			os.close();
		} catch (Exception e) {	toast(e.getMessage(),this);}
	}
	public static void loadDefaults(Activity a){
		String filename = a.getFilesDir().getPath() + File.separator + defaultFilename;
		if(!(new File(filename).exists())) return;
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
			String line = br.readLine();
			inCalibration(ioio_.leftWheelHelper, line);
			line = br.readLine();
			inCalibration(ioio_.rightWheelHelper, line);
			line = br.readLine();
			inCalibration(ioio_.gateServoHelper, line);
			line = br.readLine();
			inCalibration(ioio_.ejectServoHelper, line);
			br.close();
		} catch (Exception e) {	
			toast(e.getMessage(),a);
		}
	}
	
	private static void toast(Object o, Activity a){
		Toast.makeText(a, String.valueOf(o), Toast.LENGTH_LONG).show();
	}
	
	private String outCalibration(CalibratedMotor cm){
		String s = Integer.toString(cm.fwdMaxPulse);
		s += "," + Integer.toString(cm.revMaxPulse);
		s += "," + Integer.toString(cm.centrePulse);
		return s;
	}
	
	private static void inCalibration(CalibratedMotor cm, String line){
		List<String> items = Arrays.asList(line.split("\\s*,\\s*"));
		cm.fwdMaxPulse = Integer.parseInt(items.get(0));
		cm.revMaxPulse = Integer.parseInt(items.get(1));
		cm.centrePulse = Integer.parseInt(items.get(2));
	}
	
	private void update(){
		txtForward.setText(String.valueOf(activeCM.fwdMaxPulse));
		txtReverse.setText(String.valueOf(activeCM.revMaxPulse));
		if(isServo){
			txtCentre.setText(getString(R.string.blank));
		}else {
			txtCentre.setText(String.valueOf(activeCM.centrePulse));
		}
		ioio_.leftWheelHelper.forcePWM(0);
		ioio_.rightWheelHelper.forcePWM(0);
		ioio_.gateServoHelper.forcePWM(0);
		ioio_.ejectServoHelper.forcePWM(0);
		
		switch (myDir) {
		case FORWARD:
			pwm=activeCM.fwdMaxPulse;
			break;
		case CENTRE:
			pwm=activeCM.centrePulse;
			break;
		case REVERSE:
			pwm=activeCM.revMaxPulse;
			break;
		}
		switch (myMotor) {
		case LEFTWHEEL:
			ioio_.leftWheelHelper.forcePWM(pwm);
			break;
		case RIGHTWHEEL:
			ioio_.rightWheelHelper.forcePWM(pwm);
			break;
		case GATE:
			ioio_.gateServoHelper.forcePWM(pwm);
			break;
		case EJECT:
			ioio_.ejectServoHelper.forcePWM(pwm);
			break;
		}
//		ioio_.setLeftWheelPulse(pwm);
//		ioio_.setRightWheelPulse(pwm);
//		ioio_.setGateServoPulse(pwm);
//		ioio_.setEjectServoPulse(pwm);
		if(!ioio_.isConnected()){
			status("IOIO not Connected");
		}
		
	}
	
	private void setupUI(){
		bCpp.setEnabled(!isServo);
		bCp.setEnabled(!isServo);
		bCm.setEnabled(!isServo);
		bCmm.setEnabled(!isServo);
		
		update();
	}
	
	private void status(String s){
		txtStatus.setText(s);
	}
}