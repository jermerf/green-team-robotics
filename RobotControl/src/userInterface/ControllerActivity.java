package userInterface;

import java.util.Calendar;

import mun.greenTeam.robotcontrol.R;

import robotAI.Motion;
import ioiohw.IOIOController;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public class ControllerActivity extends Activity{
	private static IOIOController iController;
	private ControlTimer cTimer = new ControlTimer();
	public static void setIOIO(IOIOController io){
		iController=io;
	}
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.simple_controller);
		
		(new Thread(cTimer)).start();
		cTimer.changeMotion(Motion.STOP, 0);
	}
	
	public void onClick_bForward (View v){
		cTimer.changeMotion(Motion.FORWARD, 1000);
	}
	public void onClick_bTurnLeft (View v){
		cTimer.changeMotion(Motion.SPIN_LEFT, 600);
	}
	public void onClick_bTurnRight (View v){
		cTimer.changeMotion(Motion.SPIN_RIGHT, 600);
	}
	public void onClick_bReverse (View v){
		cTimer.changeMotion(Motion.BACK_UP, 1000);
	}
	public void onClick_bGate (View v){
		cTimer.changeMotion(Motion.EAT_PUCK, 1000);
	}
	public void onClick_bEject (View v){
		cTimer.changeMotion(Motion.EJECT_PUCK, 1000);
	}
	public void onClick_bBarrel (View v){
		cTimer.changeMotion(Motion.SPIN_LEFT, 2500);
	}
	
	private class ControlTimer implements Runnable{
		private long startTime = 0;
		private int duration = 0;
		public void changeMotion(Motion m, int howLong){
			iController.changeAction(m);
			startTime = time();
			duration = howLong;
		}
		
		@Override
		public void run() {
			while(true){
				while(time()<startTime  + duration){}
				iController.changeAction(Motion.STOP);
				try{
					Thread.sleep(50);
				}catch(Exception e){ }
			}
		}
		
		private long time(){
			return Calendar.getInstance().getTimeInMillis();
		}
	}
}
