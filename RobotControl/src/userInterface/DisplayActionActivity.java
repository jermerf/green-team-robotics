package userInterface;

import org.opencv.android.*;

import ioio.lib.util.IOIOLooper;
import ioio.lib.util.android.IOIOActivity;
import robotAI.StateMachine;
import mun.greenTeam.robotcontrol.R;
import android.R.bool;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import robotAI.*;
import sensors.SensorHandler;
import vision.*;
import ioiohw.*;

public class DisplayActionActivity extends IOIOActivity {
	private boolean paused = false;
	private TextView txtState;
	private TextView txtStatus;
	private TextView txtIOIOStatus;
	private TextView txtMotors;
	public static volatile String stateInfo = "----";
	public static volatile String status = "----";
	public static volatile String ioioStatus = "IOIO not connected";
	public static volatile String motorStatus = "Motors";
		
	StateMachine mStateMachine;
	vision.ViewGrabber mViewGrabber;
	IOIOController mIOIO_Controller;
	SensorHandler sensors;
	private IOIOHardware ioio_;
	
	private JavaCameraCustom cvRearCamera;
	private CameraListener camListener = new CameraListener();
	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
		public void onManagerConnected(int status) {
			switch (status) {
			case LoaderCallbackInterface.SUCCESS:
				cvRearCamera.enableView();
				break;
			default:
				break;
			}
			super.onManagerConnected(status);
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_display_action);
		
		ioio_ = new IOIOHardware();
		mViewGrabber = (ViewGrabber)camListener;
		mIOIO_Controller = new IOIOController(ioio_);
		sensors = new SensorHandler(this);
		mStateMachine = new robotAI.StateMachine( (IOIOInterface)mIOIO_Controller, mViewGrabber, sensors);
		cvRearCamera = (JavaCameraCustom)findViewById(R.id.cvRearCamera);
		cvRearCamera.setCvCameraViewListener(camListener);
		CalibrationAcivity.setIOIOHardware(ioio_);
		ControllerActivity.setIOIO(mIOIO_Controller);
		CalibrationAcivity.loadDefaults(this);
		initStatusUpdater();
		Thread newThread = new Thread(mStateMachine);
		newThread.start();
	}
	
	protected void onResume() {
		super.onResume();
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, mLoaderCallback);
	}
	
	private void initStatusUpdater(){
		txtState = (TextView)findViewById(R.id.actionIndicator);
		txtStatus = (TextView)findViewById(R.id.statusIndicator);
		txtIOIOStatus = (TextView)findViewById(R.id.ioioStatus);
		txtMotors = (TextView)findViewById(R.id.txtMotors);
		Thread statusUpdater = new Thread(new Runnable() {
			@Override
			public void run() {
				while(true){
					try {
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								txtState.setText(stateInfo);
								txtStatus.setText(status);
								txtIOIOStatus.setText(ioioStatus);
								txtMotors.setText(motorStatus);
								updateFlashMode();
							}
						});
						Thread.sleep(10);
					} catch (Exception e) {
						status = e.getMessage();
					}
				}
			}
		});
		statusUpdater.start();
	}

	private void updateFlashMode(){
		if(currentFlashSate != flashOn){
			cvRearCamera.setFlashMode(flashOn);
			currentFlashSate = flashOn;
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.display_action, menu);
		return true;
	}

	public void onClick_buttonTogglePause(View view){
		Button pauseButton = (Button) view;
		if(paused){
			pauseButton.setText("Pause");
			resume();
			// Code that will resume the robot's AI
		}else{
			pauseButton.setText("Resume");
			pause();
			// Code that will pause the robot's AI
		}
		paused = !paused;
	}
	
	private void pause(){
		ioio_.pause();
		mStateMachine.pause = true;
		mIOIO_Controller.pause=true;
	}
	
	private void resume(){
		ioio_.resume();
		mStateMachine.pause = false;
		mIOIO_Controller.pause=false;
	}
	
	public void onClick_buttonCalibrateMotor(View v){
		mStateMachine.pause = true;
		mIOIO_Controller.pause=true;
		Intent calibrationIntent = new Intent(this, CalibrationAcivity.class);
		this.startActivity(calibrationIntent);
	}
	
	public void onClick_bController(View v){
		mStateMachine.pause = true;
		Intent controllerIntentIntent = new Intent(this, ControllerActivity.class);
		this.startActivity(controllerIntentIntent);
	}
	
	private boolean flashOn=false;
	private boolean currentFlashSate = false;
	public void onClick_bFlash(View v){
		flashOn = !flashOn;
	}
	protected IOIOLooper createIOIOLooper() {
		//status = "Getting IOIO Looper";
		return ioio_;
	}
	
}