package vision;

import java.util.ArrayList;

public interface ViewGrabber {
	
	// returns an arraylist of Visibleobjects - the things the robot can see
	// a cluster of pucks counts as 1 object
	public ArrayList<robotAI.VisibleObject> getObjectsInView();
	
	// returns true if there is a puck in the grabber
	boolean gotAPuck();
	
	/* int corresponds to the color of the puck in the grabber
	 * 0 - there aint no puck there
	 * 1 - GREEN
	 * 2 - PINK
	*/
	int getPuckColor();
	
}
