package vision;

import org.opencv.core.Scalar;

public class ColourTarget {
	public Scalar targetColourLower;
	public Scalar targetColourUpper;
	public int type;
	public int minArea;
	public ColourTarget(int type_,int h,int s,int v,boolean andAbove){
		if(andAbove){
			targetColourLower = new Scalar(h,s,v,0);
			targetColourUpper = new Scalar(255,255,255,255);
		}else{
			targetColourLower = new Scalar(0,0,0,0);
			targetColourUpper = new Scalar(h,s,v,255);
		}
		type = type_;
	}
	
	public ColourTarget(int type_,int minArea_, int h1,int s1,int v1,int h2,int s2,int v2){
		int ha,sa,va,hb,sb,vb;
		if(h1<h2) { ha=h1; hb=h2; }else{ hb=h1; ha=h2; }
		if(s1<s2) { sa=s1; sb=s2; }else{ sb=s1; sa=s2; }
		if(v1<v2) { va=v1; vb=v2; }else{ vb=v1; va=v2; }
		targetColourLower = new Scalar(ha,sa,va,0);
		targetColourUpper = new Scalar(hb,sb,vb,255);
		type = type_;
		minArea = minArea_;
	}
	
	@Override
	public String toString(){
		double[] low = targetColourLower.val;
		double[] high = targetColourUpper.val;
		return"HSV from " + low[0] + "," + low[1] + "," + low[2] +"," + low[3] + 
				" to " + high[0] + "," + high[1] + "," + high[2] + "," + high[3];
	}
	
}
