package vision;

import java.util.ArrayList;
import java.util.List;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;

import android.R.integer;
import android.util.Log;

import robotAI.VisibleObject;
import userInterface.DisplayActionActivity;

public class CameraListener implements CvCameraViewListener, ViewGrabber {
	public ArrayList<ColourTarget> colourTargets;
	private volatile ArrayList<VisibleObject> visibleObjects;
	private ArrayList<UnknownBlob> blobs;
	private ArrayList<MatOfPoint> relevantContours;
	private int contourColor = 100;
	private String tag = "--JER--";
	
	public CameraListener(){
		colourTargets = new ArrayList<ColourTarget>();
		colourTargets.add(new ColourTarget(VisibleObject.UNKNOWN_PUCK_GREEN, 100, 60, 130, 60, 
																		120, 255, 255));
		Log.i(tag, colourTargets.get(0).toString());
		colourTargets.add(new ColourTarget(VisibleObject.OTHER_ROBOT,VisibleObject.FRAME_PIXEL_COUNT/100,
																		140, 80, 50, 
																		220, 255, 255));
		colourTargets.add(new ColourTarget(VisibleObject.WALL,VisibleObject.FRAME_PIXEL_COUNT/50,
																			0,   0,   0,
																			255, 255, 70));
		
	}
	
	@Override
	public void onCameraViewStarted(int width, int height) { }
	@Override
	public void onCameraViewStopped() { }
	
	/*	************************************************************************************
	 * For each colourTarget in colourTargets
	 * 		Add unknownBlob of that colour to blobs
	 * 
	 *************************************************************************************/
	int frames = 0;
	@Override
	public Mat onCameraFrame(Mat ifr) {
		ArrayList<VisibleObject> tmpObjects = new ArrayList<VisibleObject>();
		blobs = new ArrayList<UnknownBlob>();
		relevantContours = new ArrayList<MatOfPoint>();
		Mat ifrHSV = new Mat();
		Imgproc.cvtColor(ifr, ifrHSV, Imgproc.COLOR_RGB2HSV_FULL);
		
		for(int i=0;i<colourTargets.size();i++){
			recognizeWithColor(ifrHSV, colourTargets.get(i));
		}
		
		for(int i=0;i<blobs.size();i++){
			UnknownBlob blob = blobs.get(i);
//			status(blob.toString());
			drawContourCenter(ifr, blob);
			relevantContours.add(blob.myContour);
			VisibleObject vObj = new VisibleObject(blob.type, blob.x-320, 480-blob.y);
//			status(vObj.toString());
			vObj.setPixelArea(blob.pxArea);
			tmpObjects.add(vObj);
		}
		
		contourColor += 50;
		if(contourColor > 250) contourColor = 100;
		Imgproc.drawContours(ifr, relevantContours, -1, new Scalar(contourColor,contourColor,contourColor,255));
		if(tmpObjects.size()==0){
			drawFrame(ifr);
		}
		
		visibleObjects = tmpObjects;
//		listObjects();

		//Cleanup
		ifrHSV.release();
		return ifr;
	}
	
	private void listObjects(){
		String s = "";
		int uGreen = 0;
		int uPink = 0;
		int walls = 0;
		int sPucks = 0;
		int gPucks = 0;
		int oRobot = 0;
		for(int i=0;i<visibleObjects.size();i++){
			switch (visibleObjects.get(i).getType()) {
				case VisibleObject.UNKNOWN_PUCK_GREEN: uGreen++; break;
				case VisibleObject.UNKNOWN_PUCK_PINK: uPink++; break;
				case VisibleObject.WALL: walls++; break;
				case VisibleObject.SINGLE_PUCK: sPucks++; break;
				case VisibleObject.PUCK_GROUP: gPucks++; break;
				case VisibleObject.OTHER_ROBOT: oRobot++; break;
			}
		}
		s += "uGreen:" + uGreen
			+ "\nuPink:" + uPink
			+ "\nwalls:" + walls
			+ "\nsPucks:" + sPucks
			+ "\ngPucks:" + gPucks
			+ "\noRobot:" + oRobot;
		status(s);
	}
	
	int times = 0;
	private void recognizeWithColor(Mat ifrHSV, ColourTarget target){
		//Log.i(tag, "Times: " + ++times);
		Mat mMask = new Mat();
		Mat mDilatedMask = new Mat();
		List<MatOfPoint> contoursList = new ArrayList<MatOfPoint>();
		
		Core.inRange(ifrHSV, target.targetColourLower, target.targetColourUpper, mMask);
		Imgproc.dilate(mMask, mDilatedMask, new Mat());
		Imgproc.findContours(mDilatedMask, contoursList, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);

		for(int i=0; i< contoursList.size(); i++){
			if(Imgproc.contourArea(contoursList.get(i)) < target.minArea) continue;
			blobs.add(makeBlob(contoursList.get(i), target.type));
		}
		
		mMask.release();
		mDilatedMask.release();
	}
	
	private UnknownBlob makeBlob(MatOfPoint contour, int type){
		Moments moments = Imgproc.moments(contour);
		int x = (int)(moments.get_m10()/moments.get_m00());
		int y = (int)(moments.get_m01()/moments.get_m00());
		return new UnknownBlob(x, y, (int)moments.get_m00(), contour, type);
	}
	
	private void drawContourCenter(Mat canvas,UnknownBlob blob){
		int xl,xh,yl,yh;
		int pSize = 5;
		xl = blob.y - pSize;
		xh = blob.y + pSize;
		yl = blob.x - pSize;
		yh = blob.x + pSize;
		if(xl < 0) xl = 0;
		if(xh > canvas.rows()) xh = canvas.rows();
		if(yl < 0) yl = 0;
		if(yh > canvas.cols()) yh = canvas.cols();
		Mat drawSquare = canvas.submat(xl, xh, yl, yh);
		drawSquare.setTo(new Scalar(contourColor, contourColor, contourColor, 255));
		
	}
	
	private void drawFrame(Mat canvas){
		Scalar color = new Scalar(255, 25, 25, 255);
		int thickness = 3;
		Mat tmp = canvas.submat(0, canvas.rows(), 0, thickness);
		tmp.setTo(color);
		tmp = canvas.submat(0, thickness, 0, canvas.cols());
		tmp.setTo(color);
		tmp = canvas.submat(canvas.rows()-thickness, canvas.rows(), 0, canvas.cols());
		tmp.setTo(color);
		tmp = canvas.submat(0, canvas.rows(), canvas.cols()-thickness, canvas.cols());
		tmp.setTo(color);
		tmp.release();
	}
	
	public int getOffset(){
		return 0;
	}
	
	@Override
	public ArrayList<VisibleObject> getObjectsInView() {
		if(visibleObjects==null) return null;
		if(visibleObjects.size()==0) return null;
		return visibleObjects;
	}
	int puckCounter = 0;
	public boolean gotAPuck() {
		return false;
	}
	@Override
	public int getPuckColor() {
		// TODO Auto-generated method stub
		return 0;
	}
	private void status(String msg){
		DisplayActionActivity.ioioStatus = msg;
	}
}