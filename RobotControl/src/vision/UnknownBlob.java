package vision;

import org.opencv.core.MatOfPoint;


import robotAI.Color;
import robotAI.VisibleObject;
import robotAI.constants;

public class UnknownBlob {
	public int x,y;
	public int pxArea;
	public MatOfPoint myContour;
	public int type;
	public int radius;
	public int realScale;
	public Color mColor= Color.NONE; 
	
	public UnknownBlob(int x_,int y_,int pxArea_,MatOfPoint myContour_,int type_){
		x = x_;
		y = y_;
		pxArea = pxArea_;
		myContour = myContour_;
		radius = (int)Math.sqrt(pxArea/3); // Represents the opposite of piR^2 but is slightly larger
		realScale = toRealWorldScale();
		type = type_;
		if(type_==VisibleObject.UNKNOWN_PUCK_GREEN){
			if(realScale > constants.realClusterSizeMinimum){
				type = VisibleObject.PUCK_GROUP;
			}else{
				type = VisibleObject.SINGLE_PUCK;
			}
			mColor = Color.GREEN;
		}else if(type ==VisibleObject.UNKNOWN_PUCK_PINK){
			if(realScale > constants.realClusterSizeMinimum){
				type = VisibleObject.PUCK_GROUP;
			}else{
				type = VisibleObject.SINGLE_PUCK;
			}
			mColor = Color.PINK;
		}
	}
	
	public int toRealWorldScale(){
		return pxArea/y + (int)(-0.001800466965*y*y+0.4844897114*y+12.35060444);
	}
	
	@Override
	public String toString(){
		return "X: " + x + "\nY: " + y + "\nArea:\n " + pxArea + "\nReal Size:\n" + realScale;
	}
}
