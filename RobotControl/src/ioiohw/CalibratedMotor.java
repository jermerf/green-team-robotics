package ioiohw;

public class CalibratedMotor {
	public int centrePulse = 0;
	public int fwdMaxPulse = 0;
	public int revMaxPulse = 0;
	private int targetPulse;
	private int currentPulse;
	public String lastAction = "";
	public CalibratedMotor(int forwardMaxInMicroseconds, int reverseMaxInMicroseconds){
		centrePulse = (forwardMaxInMicroseconds + reverseMaxInMicroseconds)/2;
		fwdMaxPulse = forwardMaxInMicroseconds;
		revMaxPulse = reverseMaxInMicroseconds;
	}
	
	public CalibratedMotor(int centrePulseInMicroseconds, int forwardMaxInMicroseconds, int reverseMaxInMicroseconds){
		centrePulse = centrePulseInMicroseconds;
		fwdMaxPulse = forwardMaxInMicroseconds;
		revMaxPulse = reverseMaxInMicroseconds;
	}
	
	public void center() { targetPulse = centrePulse;}
	public void forwardMax() { 
		targetPulse = fwdMaxPulse;
		percentOn = 100;
		lastAction = "Forward";
	}
	public void reverseMax() { 
		targetPulse = revMaxPulse;
		percentOn = 100;
		lastAction = "Forward";
	}
	public void stop(){
		targetPulse = 0;
		percentOn = 100;
	}
	
	int percentOn = 0;
	int count = 0;
	public void goForwardPercent(int percent){
		percentOn = percent;
		targetPulse = fwdMaxPulse;
	}
	public void goReversePercent(int percent){
		percentOn = percent;
		targetPulse = revMaxPulse;
	}	
	
	protected int getTargetPulse(){ 
		count++;
		count = count % 25;
		if(count*4 > percentOn){
			currentPulse = centrePulse;
		}else{
			currentPulse = targetPulse;
		}
		return currentPulse;
	}
	
	public int getCurrentPulse(){
		return currentPulse;
	}
	
	public void forcePWM(int pwm){
		targetPulse = pwm;
		percentOn = 100;
	}
}
