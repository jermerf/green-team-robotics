package ioiohw;

import java.util.Calendar;

import robotAI.Motion;

public class IOIOController implements robotAI.IOIOInterface{
	private volatile Motion currentMotion = Motion.STOP;
	private volatile boolean gripperBusy = false;
	private IOIOActionTimer actionTimer;
	private IOIOHardware ioio_;
	public boolean pause = false;
	// change the current action of the robot
	
	public IOIOController(IOIOHardware ioio_inst) {
		ioio_ = ioio_inst;
	}
	
	public IOIOHardware ioioHw(){
		return ioio_;
	}
	
	public void changeAction(Motion newMotion) {
		currentMotion = newMotion;
		startIOIOTimer();
		actionTimer.newAction();
	}

	// returns true if the robot is busy eating or ejecting a puck
	@Override
	public boolean isGripperBusy() {
		return gripperBusy;
	}
	
	public void startIOIOTimer(){
		if(actionTimer==null){
			actionTimer = new IOIOActionTimer();
			Thread actionThread = new Thread(actionTimer);
			actionThread.start();
		}
	}
	
	class IOIOActionTimer implements Runnable{
		private volatile boolean hasNewAction = true;
		private long startTime;
		private final int eatTime = 600;
		private final int ejectTime = 500;
		private final int reverseTime = 600;
		private int actionSequence = 0;
		private Motion motionInProgress = Motion.STOP;
		
		public void run() {
			while(true){
				while(pause){
					try {
						Thread.sleep(100);
					} catch (Exception e) { }
				}
				if(hasNewAction){
					hasNewAction = false;
					ioio_.leftWheelHelper.stop();
					ioio_.rightWheelHelper.stop();
					ioio_.gateServoHelper.stop();
					ioio_.ejectServoHelper.stop();

					switch (currentMotion) {
					case EJECT_PUCK:{
						startTime = time();
						motionInProgress = currentMotion;
						actionSequence = 0;
						ioio_.ejectServoHelper.forwardMax();
						break;
					}
					case EAT_PUCK:{	
						startTime = time();
						motionInProgress = currentMotion;
						actionSequence = 0;
						ioio_.leftWheelHelper.forwardMax();
						ioio_.rightWheelHelper.forwardMax();
						ioio_.gateServoHelper.forwardMax();
						break;
					}
					case FORWARD:{
						ioio_.leftWheelHelper.forwardMax();
						ioio_.rightWheelHelper.forwardMax();
						break;
					}
					case BACK_UP:{
						ioio_.leftWheelHelper.reverseMax();
						ioio_.rightWheelHelper.reverseMax();
						break;
					}
					case REVERSE:{
						startTime = time();
						motionInProgress = currentMotion;
						actionSequence = 0;
						ioio_.leftWheelHelper.reverseMax();
						ioio_.rightWheelHelper.reverseMax();
						ioio_.gateServoHelper.forwardMax();
						break;
					}
					case SPIN_LEFT:{
						ioio_.leftWheelHelper.reverseMax();
						ioio_.rightWheelHelper.forwardMax();
						break;
					}
					case SPIN_RIGHT:{
						ioio_.leftWheelHelper.forwardMax();
						ioio_.rightWheelHelper.reverseMax();
						break;
					}
					case STOP:{
						ioio_.leftWheelHelper.stop();
						ioio_.rightWheelHelper.stop();
						break;
					}
				}}
				hasNewAction = false;
				//status();
				while(motionInProgress != Motion.STOP){
					gripperBusy = true;
					switch(motionInProgress){
						case EJECT_PUCK:{
							switch (actionSequence) {
							case 0:{
								if(time()>startTime+ejectTime/2){
									ioio_.ejectServoHelper.reverseMax();
									actionSequence++;
								}
								break;
							}
							case 1:{
								if(time()>startTime+ejectTime){
									ioio_.ejectServoHelper.stop();									
									actionSequence++;
									motionInProgress = Motion.STOP;
								}
								break;
							}
							}
							break;
						}	
						case EAT_PUCK:{
							switch (actionSequence) {
							case 0:
								if(time()>startTime+eatTime/2){
									ioio_.leftWheelHelper.stop();
									ioio_.rightWheelHelper.stop();
									ioio_.gateServoHelper.reverseMax();
									actionSequence++;
								}
								break;
							case 1:
								if(time()>startTime+eatTime){
									ioio_.gateServoHelper.stop();
									actionSequence++;
									motionInProgress = Motion.STOP;
								break;
								}
							}
						}
						case REVERSE:{
							switch (actionSequence) {
							case 0:
								if(time()>startTime+reverseTime/2){
									ioio_.leftWheelHelper.stop();
									ioio_.rightWheelHelper.stop();
									ioio_.gateServoHelper.reverseMax();
									actionSequence++;
								break;
							}
							case 1:
								if(time()>startTime+reverseTime){
									ioio_.gateServoHelper.stop();
									actionSequence++;
									motionInProgress = Motion.STOP;
								break;
							
								}
							}
						}
						default:{
						}break;
					}
				}
			}
		}
		public void newAction(){
			hasNewAction = true;
		}
		
		private long time(){
			return Calendar.getInstance().getTimeInMillis();
		}
		
//		private void status(){
//			String s = "CM:";
//			switch(currentMotion){
//			case EAT_PUCK:
//				s+="Eat";
//				break;
//			case EJECT_PUCK:
//				s+="Eject";
//				break;
//			case FORWARD:
//				s+="Forward";
//				break;
//			case REVERSE:
//				s+="Reverse";
//				break;
//			case SPIN_LEFT:
//				s+="Left";
//				break;
//			case SPIN_RIGHT:
//				s+="Right";
//				break;
//			case STOP:
//				s+="STOP";
//				break;
//			default:
//				break;
//			}
//			
//			
//			s += "\nWL:" +ioio_.leftWheelHelper.getCurrentPulse() +
//					"\nWR:" + ioio_.rightWheelHelper.getCurrentPulse()+
//					"\nGS:" + ioio_.gateServoHelper.getCurrentPulse()+
//					"\nES:" + ioio_.ejectServoHelper.getCurrentPulse();
//			DisplayActionActivity.ioioStatus = s;
//		}
	}
}
