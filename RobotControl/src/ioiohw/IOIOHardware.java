package ioiohw;

import android.util.Log;
import userInterface.CalibrationAcivity;
import userInterface.DisplayActionActivity;
import ioio.lib.api.DigitalOutput;
import ioio.lib.api.PwmOutput;
import ioio.lib.api.exception.ConnectionLostException;
import ioio.lib.util.BaseIOIOLooper;

public class IOIOHardware extends BaseIOIOLooper{
	private boolean paused = false;
	private String tag = "--IOIO--";
	public CalibratedMotor leftWheelHelper = new CalibratedMotor(1550, 1350, 150);
	public CalibratedMotor rightWheelHelper = new CalibratedMotor(1550, 1750, 1350);
	public CalibratedMotor gateServoHelper = new CalibratedMotor(1550, 1750, 1350);
	public CalibratedMotor ejectServoHelper = new CalibratedMotor(1550, 1750, 1350);
	
	// IOIO I/O objects
	private PwmOutput ioioPWMWheelLeft;		// Pin 11
	private PwmOutput ioioPWMWheelRight;	// Pin 12	
	private PwmOutput ioioPWMServoGate;		// Pin 13
	private PwmOutput ioioPWMServoEject;	// Pin 14
	private DigitalOutput ioioDOutEnWheelLeft;		// Pin 9
	private DigitalOutput ioioDOutEnWheelRight;		// Pin 15
	private DigitalOutput ioioDOutEnServoGate;		// Pin 16
	private DigitalOutput ioioDOutEnServoEject;		// Pin 17
	
	private boolean connected = false;
	// IOIO I/O values
	
	
	// Power controls
	private boolean enPowWheelLeft = true;
	private boolean enPowWheelRight = true;
	private boolean enPowServoGate = true;
	private boolean enPowServoEject = true;
	
	protected void setup() throws ConnectionLostException, InterruptedException {
		setStatus("Setting up IOIO");
		wasSetup = true;
		ioioPWMWheelLeft = ioio_.openPwmOutput(11, 400);
		ioioPWMWheelRight = ioio_.openPwmOutput(12, 400);
		ioioPWMServoGate = ioio_.openPwmOutput(13, 400);
		ioioPWMServoEject = ioio_.openPwmOutput(14, 400);
		ioioDOutEnWheelLeft = ioio_.openDigitalOutput(9);
		ioioDOutEnWheelRight = ioio_.openDigitalOutput(15);
		ioioDOutEnServoGate = ioio_.openDigitalOutput(16);
		ioioDOutEnServoEject = ioio_.openDigitalOutput(17);	
	}
	
	public void loop() throws ConnectionLostException, InterruptedException {
		connected = true;
		wasLooped = true;
		if(paused) {
			Log.i(tag, "Pause");
			ioio_.beginBatch();{
				ioioPWMWheelLeft.setPulseWidth(0);
				ioioPWMWheelRight.setPulseWidth(0);
				ioioPWMServoGate.setPulseWidth(0);
				ioioPWMServoEject.setPulseWidth(0);
				
				ioioDOutEnWheelLeft.write(true);
				ioioDOutEnWheelRight.write(true);
				ioioDOutEnServoGate.write(true);
				ioioDOutEnServoEject.write(true);
			}ioio_.endBatch();
		}else{
			String status = "Loop:"; 
			ioio_.beginBatch();{
				//GuiActivity.setStatus(cycles++ + ": " + offset);
				ioioPWMWheelLeft.setPulseWidth(leftWheelHelper.getTargetPulse());
				ioioPWMWheelRight.setPulseWidth(rightWheelHelper.getTargetPulse());
				ioioPWMServoGate.setPulseWidth(gateServoHelper.getTargetPulse());
				ioioPWMServoEject.setPulseWidth(ejectServoHelper.getTargetPulse());
				
				// Power enabled low, hence the inversion
				ioioDOutEnWheelLeft.write(!enPowWheelLeft);
				ioioDOutEnWheelRight.write(!enPowWheelRight);
				ioioDOutEnServoGate.write(!enPowServoGate);
				ioioDOutEnServoEject.write(!enPowServoEject);
//				status += "LeftWheel\n ";
//				status += (enPowWheelLeft) ? "Enabled\n" : "Disabled\n "; 
//				status += leftWheelHelper;
//				status += "\nRightWheel\n";
//				status += (enPowWheelRight) ? "Enabled\n" : "Disabled\n"; 
//				status += pwmWheelRight;
//				status += "\nGateServo\n";
//				status += (enPowServoGate) ? "Enabled\n" : "Disabled\n"; 
//				status += pwmServoGate;
//				status += "\nEjectServo\n";
//				status += (enPowServoEject) ? "Enabled\n" : "Disabled\n";
//				status += pwmServoEject;
//				status+= "\nPWM:\n" + CalibrationAcivity.pwm;
				//setStatus(status);
			}ioio_.endBatch();
		}
	}
	
	boolean wasSetup = false;
	boolean wasLooped = false;
	@Override
	public void disconnected() {
		connected = false;
		super.disconnected();
		String disconnectMessage = "IOIO Disconnected. ";
		if(wasSetup && !wasLooped){
			disconnectMessage += "Make sure you're not using some pins for unsupported actions.";
		}
		setStatus(disconnectMessage);
	}
	public String myStatus = "";
	private void setStatus(String s){
		myStatus = s;
		DisplayActionActivity.ioioStatus = s;
	}
	
	public void pause(){
		paused = true;
	}
	
	public void resume(){
		paused = false;
	}
		
	
	public boolean isConnected(){
		return connected;
	}
}
