package org.opencv.android;


import java.util.List;

import org.opencv.android.JavaCameraView;

import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;

public class JavaCameraCustom extends JavaCameraView {
	String tag = "--JER--";
	public JavaCameraCustom(Context context, AttributeSet attrs) {
        super(context, attrs);
        setSize(640, 480);
	}
	
	public void setResolution(Size res){
		disconnectCamera();
		setSize(res.width, res.height);
		connectCamera(getWidth(), getHeight());
	}
	
	public void setSize(int w, int h){
		mMaxWidth = w;
		mMaxHeight = h;
	}
	
	public Size getResolution(){
		return mCamera.getParameters().getPreviewSize();
	}
	
	public List<Size> getResolutionList(){
		if(mCamera==null)Log.i(tag, "NULL mCamera");
		return mCamera.getParameters().getSupportedPreviewSizes();
	}
	
	public boolean setFlashMode(boolean isOn){
		if(mCamera==null){
			return false; 
		}
		mCamera.getParameters();
		Parameters p = mCamera.getParameters();
		if(isOn){
			p.setFlashMode(Parameters.FLASH_MODE_TORCH);
		}else{
			p.setFlashMode(Parameters.FLASH_MODE_OFF);
		}
		mCamera.setParameters(p);
		return true;
	}
	
	
}