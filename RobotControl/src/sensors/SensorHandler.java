package sensors;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class SensorHandler implements SensorEventListener{
    
    private SensorManager mSensorManager;
    private Sensor accelerometer;
    private Sensor proximitySensor;
    
    // accelerometer sensor value
    volatile float accelerometerX = 0;
    volatile float accelerometerY = 0;
    volatile float accelerometerZ = 0;
    // proximity sensor value (centimeters)
    volatile float distance = 0;

    public SensorHandler(Activity activity){
    	mSensorManager = (SensorManager)activity.getSystemService(Context.SENSOR_SERVICE);
    	accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    	proximitySensor = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
    }

    // returns true if this type of sensor is available on the android device
    public boolean findSensor(int sensorType){
    	if(sensorType == Sensor.TYPE_ACCELEROMETER){
    		if(accelerometer==null){return false;}
    		else{return true;}
    	}
    	if(sensorType == Sensor.TYPE_PROXIMITY){
        	if(proximitySensor==null){return false;}
        	else{return true;}
    	}
    	return false;
    }
    
    // returns true if the sensor was successfully turned on
    public boolean turnOnSensor(int sensorType){
    	if(sensorType == Sensor.TYPE_ACCELEROMETER){
    		mSensorManager.registerListener(this,accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    		return true;
    	}
    	if(sensorType == Sensor.TYPE_PROXIMITY){
    		mSensorManager.registerListener(this,proximitySensor, SensorManager.SENSOR_DELAY_NORMAL);
    		return true;
    	}
    	return false;
    }
    
    /// get values from sensors
    public float getAccelerometerX(){
    	return accelerometerX;
    }
    public float getAccelerometerY(){
    	return accelerometerY;
    }
    public float getAccelerometerZ(){
    	return accelerometerZ;
    }
    float getDistance(){
    	return distance;
    }
    
	public void onSensorChanged(SensorEvent event) {
		if(event.sensor==accelerometer){
			accelerometerX = event.values[0];
			accelerometerY = event.values[1];
			accelerometerZ = event.values[2];
		}
		if(event.sensor==proximitySensor){
			distance = event.values[0];
		}
	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub
	}
}