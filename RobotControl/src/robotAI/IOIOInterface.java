package robotAI;

import ioiohw.IOIOHardware;

public interface IOIOInterface {
	
	// change the current action of the robot
	public abstract void changeAction(Motion newMotion);
	
	// returns true if gripper is busy opening or closing
	boolean isGripperBusy();
	public IOIOHardware ioioHw();
	
	// these are for getting encoder values, will implement later
//	public abstract int getRightDistance();
//	public abstract int getLeftDistance();
}
