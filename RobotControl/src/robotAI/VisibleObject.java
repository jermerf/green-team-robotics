package robotAI;

public class VisibleObject {
	public static final int FRAME_PIXEL_COUNT = 640*480;
	int distance;
	int angle;
	int type;
	int x;
	int y;
	int pixelArea=0;
	Color color;
	
	public static final int OTHER_ROBOT = 0;
	public static final int SINGLE_PUCK = 1;
	public static final int PUCK_GROUP = 2;
	public static final int WALL = 3;
	public static final int UNKNOWN_PUCK_GREEN = 4;
	public static final int UNKNOWN_PUCK_PINK = 5;
	
	
//	public VisibleObject(int inType){
//		this.type = inType;
//		this.distance = 10;
//		this.angle = 0;
//	}
	
	public VisibleObject(int inType, int inX, int inY){
		this.type = inType;
		this.distance = 10;
		this.x = inX;
		this.y = inY;
		this.angle = x;
	}

	public int getType(){
		return type;
	}
	
	void setColor(Color inColor){
		color = inColor;
	}
	
	Color getColor(){
		return color;
	}
	
	public void setDistance(int dist){
		distance = dist;
	}
	// estimates distance away from the front of the robot
	public int getDistance(){
		return (int)Math.sqrt(x*x+y*y);
	}
	
//	public void setAngle(int ang){
//		angle = ang;
//	}
	// angle relative to strait line in front of robot
	public int getAngle(){
		return angle;
	}
	
//	public void setX(int inX){
//		this.x = inX;
//	}
	public int getX(){
		return x;
	}
	
//	public void setY(int inY){
//		this.y = inY;
//	}
	public int getY(){
		return y;
	}
	public void setPixelArea(int pixels){
		pixelArea = pixels;
	}
	
	public int getPixelArea(){
		return pixelArea;
	}
	
	public String toString(){
		return "X: " + x + "\nY: " + y + "\nArea:\n " + pixelArea;
	}
}
