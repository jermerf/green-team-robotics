package robotAI;
/*
 * This class is no longer used
 */
import userInterface.DisplayActionActivity;

public class CollisionEstimator {
	
	// constructor
	CollisionEstimator(){
		// do stuff
	}
	
	// returns the motion required to collide with the object
	Motion collide(VisibleObject target){
		int type = target.getType();
		//DisplayActionActivity.status = ("got here");
		switch(type){
		// Other Robot - why would you want to collide with another robot?
		case 0:{
			return Motion.STOP;
		}
		// Single Puck
		case 1:{
//			String string = String.valueOf(target.getAngle());
//			DisplayActionActivity.status = (string);
			if(target.getAngle() > constants.singlePuckMaxAngle){ return Motion.SPIN_RIGHT; }
			else if(target.getAngle() < constants.singlePuckMinAngle){ return Motion.SPIN_LEFT; }
			else{ return Motion.FORWARD; }
		}
		// Puck Group
		case 2:{
			if(target.getAngle() > constants.puckGroupCollideMaxAngle){ return Motion.SPIN_RIGHT; }
			else if(target.getAngle() < constants.puckGroupCollideMinAngle){ return Motion.SPIN_LEFT; }
			else{ return Motion.FORWARD; }
		}
		// Wall - why would you want to collide with a wall?
		case 3:{
			return Motion.STOP;
		}
		}
		// mystery object
		return Motion.STOP;
	}
	
	/* Determines if the robot will collide with the given object
	 * Returns STOP if no collision is expected
	 * If a collision is expected, returns the submotion recommended for avoiding it
	 */
	Motion avoidCollision(VisibleObject obstacle){
		int type = obstacle.getType();
		
		switch(type){
		// Other robot
		case 0:{
			if(obstacle.getAngle() > constants.puckGroupAvoidMaxAngle){ return Motion.SPIN_LEFT; }
			else if(obstacle.getAngle() < constants.puckGroupAvoidMinAngle){ return Motion.SPIN_RIGHT; }
			else{ return Motion.STOP; }
		}
		// single puck - why would we want to avoid a single puck?
		case 1:{
			return Motion.STOP;
		}
		// puck group
		case 2:{
			if(obstacle.getAngle() > constants.puckGroupAvoidMaxAngle){ return Motion.SPIN_LEFT; }
			else if(obstacle.getAngle() < constants.puckGroupAvoidMinAngle){ return Motion.SPIN_RIGHT; }
			else{ return Motion.STOP; }
		}
		// wall
		case 3:{
//			String y = String.valueOf(obstacle.getX());
//			String pixels = String.valueOf(obstacle.getPixelArea());
//			DisplayActionActivity.status = ("y: " + y);
			if(obstacle.getY() < constants.wallYThreshold
//					&& obstacle.getPixelArea() > constants.wallThresholdPixelSize
					){
				DisplayActionActivity.status = "got here";
				if(obstacle.getX() < 0){ return Motion.SPIN_RIGHT; }
				else{ return Motion.SPIN_LEFT; }
			}
			return Motion.STOP;
		}
		}
		// mystery object
		return Motion.STOP;
	}
	
}
