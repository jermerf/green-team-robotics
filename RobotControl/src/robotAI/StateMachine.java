package robotAI;

import robotAI.IOIOInterface;
//import robotAI.Color;
import robotAI.CurrentState;
import sensors.SensorHandler;
import userInterface.DisplayActionActivity;
import vision.ViewGrabber;

import ioiohw.CalibratedMotor;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

import android.hardware.Sensor;

public class StateMachine implements Runnable {

        private IOIOInterface mIoioInterface;
        private vision.ViewGrabber mViewGrabber;
        private SensorHandler mSensorHandler;
        private static CurrentState mCurrentState;
        //Motion mSubmotion;
        //Color targetPuckColor;
        //Color myPuckColor;
        
        public volatile boolean pause;
        private boolean accelerometerActive = false;
        
        private boolean isBusy = false;
        private long startTime = 0;
        private int duration = 0;
        VisibleObject nearestPuckObject = null;

        private ArrayList<VisibleObject> objectsInView;

        public StateMachine(IOIOInterface inIOIO_Controller, ViewGrabber inViewGrabber, SensorHandler sensors) {
                mIoioInterface = inIOIO_Controller;
                mViewGrabber = inViewGrabber;
                mSensorHandler = sensors;
                
                mCurrentState = CurrentState.PUCK_SEEK;
                //mSubmotion = Motion.STOP;
                //myPuckColor = Color.NONE;
                //targetPuckColor = Color.NONE;
                
                pause = false;
        }


        @Override
        public void run() {
        	
        	// Turn on accelerometer
        	boolean found = mSensorHandler.findSensor(Sensor.TYPE_ACCELEROMETER);
        	if(found){
        		accelerometerActive = mSensorHandler.turnOnSensor(Sensor.TYPE_ACCELEROMETER);
        	}
        	
        	while(true){
        		
        		//Do I pause
        		while(pause){
        			try {
						Thread.sleep(100);
					} catch (Exception e) { }
        		}
        		
        		// Test accelerometer
        		if(accelerometerActive){
        			float x = mSensorHandler.getAccelerometerX();
                	//float y = mSensorHandler.getAccelerometerY();
                	//float z = mSensorHandler.getAccelerometerZ();
                	String acc = "x:" + x;
                	DisplayActionActivity.status = acc;
        		}
        		
        		objectsInView = mViewGrabber.getObjectsInView();
        		
        		boolean stateChanged = false;
        		if(objectsInView != null){
	        		for(int i=0;i<objectsInView.size();i++){
	        			VisibleObject obj = objectsInView.get(i);
	        			if(obj.type == VisibleObject.WALL){
	        				if(obj.y<constants.wallYThreshold){
	        					mCurrentState = CurrentState.AVOIDING_OBSTACLE;
	        					stateChanged = true;
	        				}
	        			}else if (obj.type==VisibleObject.OTHER_ROBOT) {
							if(distance(obj)<constants.otherRobotDistThreshold){
								mCurrentState = CurrentState.AVOIDING_OBSTACLE;
								stateChanged = true;
							}
						}else if (obj.type==VisibleObject.PUCK_GROUP) {
							if(distance(obj)<constants.otherRobotDistThreshold){
								mCurrentState = CurrentState.AVOIDING_OBSTACLE;
								stateChanged = true;
							}
						}else if (obj.type==VisibleObject.SINGLE_PUCK) {
							mCurrentState = CurrentState.PUCK_HOMING;
							if(nearestPuckObject==null){
								nearestPuckObject = obj;
							}else if (distance(nearestPuckObject)>distance(obj)) {
								nearestPuckObject = obj;
							}
						}
	        		}
        		}
        		
        		//Handle state changes, initialize states boo
        		if(stateChanged && !isBusy){
        			switch (mCurrentState) {
    				case AVOIDING_OBSTACLE:
    					isBusy = true;
    					startTime = time();
    					duration = constants.delayTurn90;
    					mIoioInterface.changeAction(Motion.SPIN_LEFT);
    					break;
    				case PUCK_HOMING:
    					break;
    				default:
    					break;
    				}	
        		}

        		//Handle current state
        		switch (mCurrentState) {
        		case PUCK_SEEK:
        			mIoioInterface.changeAction(Motion.FORWARD);
        			break;
				case CLUSTER_SEEK:
					mIoioInterface.changeAction(Motion.FORWARD);
					break;
				case AVOIDING_OBSTACLE:
					if(time()>startTime+duration){
						mCurrentState = CurrentState.PUCK_SEEK;
						isBusy = false;
					}
				case PUCK_HOMING:
					if(nearestPuckObject==null) break;
					
					break;
				default:
					break;
				}

        		switch (mCurrentState) {
				case AVOIDING_OBSTACLE:
					DisplayActionActivity.stateInfo = "AVOIDING OBSTACLE";
					break;
				case PUCK_SEEK:
					DisplayActionActivity.stateInfo = "LOOKING FOR A PUCK";
					break;
				case PUCK_HOMING:
					//DisplayActionActivity.stateInfo = "FOUND A PUCK. MINE!";
					if(nearestPuckObject==null) break;
					if(nearestPuckObject.x>100){
						mIoioInterface.changeAction(Motion.SPIN_RIGHT);
					}else if (nearestPuckObject.x<-100) {
						mIoioInterface.changeAction(Motion.SPIN_LEFT);
					}else{
						mIoioInterface.changeAction(Motion.FORWARD);
					}
					break;
				default:
					break;
				}
        		if(nearestPuckObject!=null){
        			DisplayActionActivity.stateInfo = "X: " + nearestPuckObject.x;
        		}
        		CalibratedMotor left = mIoioInterface.ioioHw().leftWheelHelper;
        		CalibratedMotor right = mIoioInterface.ioioHw().rightWheelHelper;
        		DisplayActionActivity.motorStatus = "LEFT:" + left.lastAction 
        										+ "\nRIGHT:" + right.lastAction;
        		nearestPuckObject=null;
        	}
        }
        
        void setSubmotion(Motion motion){
                if(motion == Motion.STOP){
                        mIoioInterface.changeAction(Motion.STOP);
                        //mSubmotion = Motion.STOP;
                }
                if(motion == Motion.FORWARD){
                        mIoioInterface.changeAction(Motion.FORWARD);
                        //mSubmotion = Motion.FORWARD;
                }
                if(motion == Motion.SPIN_LEFT){
                        mIoioInterface.changeAction(Motion.SPIN_LEFT);
                        //mSubmotion = Motion.SPIN_LEFT;
                }
                if(motion == Motion.SPIN_RIGHT){
                        mIoioInterface.changeAction(Motion.SPIN_RIGHT);
                        //mSubmotion = Motion.SPIN_RIGHT;
                }
                if(motion == Motion.REVERSE){
                        mIoioInterface.changeAction(Motion.REVERSE);
                        //mSubmotion = Motion.REVERSE;
                }
        }
        
        // returns the index of the closest object in the list of visible objects
        // returns -1 if there are no objects in the list
        int getClosestObjectIndex(ArrayList<VisibleObject> list){
                if(list == null || list.size() == 0){return -1;}
                else{
                        int closestPuckIndex = -1;
                        int closestDistance = -1;
                        for(int i=0; i<list.size(); i++){
                                if(closestDistance == -1 || list.get(i).getDistance() < closestDistance){
                                        closestPuckIndex = i;
                                        closestDistance = list.get(i).getDistance();
                                }
                        }
                        return closestPuckIndex;
                }
        }
        
        // No longer used
        class SeekerAlgorithm{
                
                long startTime;
                Random rnd;
                
                SeekerAlgorithm(){
                        rnd = new Random();
                        startTime = time();
                }
                        
                // updates the motion of the robot (either going strait or random turn)
                // returns whether the motion has changed or not
                void updateMotion(){
        
                        //setSubmotion(Motion.FORWARD);
                        
//                      /****** if we are currently going strait ******/
//                      if(mSubmotion == Motion.FORWARD){
//                              if(time() > (startTime + constants.straitTime)){        // we should turn now
//                                      int direction = rnd.nextInt(2); // choose randomly between left and right
//                                      if(direction == 0){
//                                              setSubmotion(Motion.SPIN_LEFT);
//                                              DisplayActionActivity.stateInfo = "puck seek: spin left";
//                                              }
//                                      else{
//                                              setSubmotion(Motion.SPIN_RIGHT);
//                                              DisplayActionActivity.stateInfo = "puck seek: spin right";
//                                              }
//                                      startTime = time();
//                                      
//                                      //turnTime = turnMinTime + turnMultiplier*(rnd.nextInt(turnVariable));
//                              }
//                      }
//                      /****** if we are currently turning ******/
//                      else{
//                              if(time() > startTime + constants.turnTime){    // we should go strait now
//                                      setSubmotion(Motion.FORWARD);
//                                      startTime = time();
//                                      DisplayActionActivity.stateInfo = "puck seek: forward";
//                              }
//                      }
                }
                
                void reset(){
                        startTime = time() - 1000;
                        setSubmotion(Motion.FORWARD);
                        DisplayActionActivity.stateInfo = "puck seek: forward";
                }
        }
        
        private long time(){
                return Calendar.getInstance().getTimeInMillis();
        }
        
        private double distance(VisibleObject vo){
        	return Math.sqrt(vo.x*vo.x + vo.y*vo.y);
        }

}
