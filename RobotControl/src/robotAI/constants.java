package robotAI;

public class constants {
	
	public static final long straitTime = 2500;		// robot will go strait for 2.5 seconds
	public static final long turnTime = 500;
	public static final long backupTime = 2500;
	
	// Sweet spot is a spot right in front of the robot.
	// Once a puck is here a grab motion will be activated.
	public static final int sweetSpotX = 0;
	public static final int sweetSpotY = 120;
	public static final int tolerance = 40;
	
	// If a puck cluster is in this area in front of the robot we will deposit a puck here
	public static final int depositAreaX = 0;
	public static final int depositAreaY = 140;
	public static final int depositTolerance = 40;
	
	// The boundary in real world scale between a cluster and a single puck
	public static final int realClusterSizeMinimum = 1000;

	/********************************* Collision Detection *************************************/
	
	public static final int singlePuckMinAngle = -60;
	public static final int singlePuckMaxAngle = 60;
	
	public static final int puckGroupCollideMinAngle = -50;
	public static final int puckGroupCollideMaxAngle = 50;
	
	public static final int puckGroupAvoidMinAngle = -30;
	public static final int puckGroupAvoidMaxAngle = 30;
	
	public static final int otherRobotMinAngle = -80;
	public static final int otherRobotMaxAngle = 80;
	public static final int otherRobotDistThreshold = 100;
	
	public static final int wallThresholdPixelSize = 375;
	public static final int wallYThreshold = 400;
	
	public static final int delayTurn90 = 350;
	
}
