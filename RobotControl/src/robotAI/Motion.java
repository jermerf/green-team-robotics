package robotAI;

public enum Motion {
	FORWARD,
	REVERSE,
	SPIN_RIGHT,
	SPIN_LEFT,
	EAT_PUCK,
	EJECT_PUCK,
	BACK_UP,
//	TURN_RIGHT,		future work
//	tURN_LEFT,
	STOP;
}
