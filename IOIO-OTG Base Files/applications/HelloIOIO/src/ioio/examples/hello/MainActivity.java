package ioio.examples.hello;

import ioio.lib.api.DigitalInput;
import ioio.lib.api.DigitalOutput;
import ioio.lib.api.PwmOutput;
import ioio.lib.api.exception.ConnectionLostException;
import ioio.lib.util.BaseIOIOLooper;
import ioio.lib.util.IOIOLooper;
import ioio.lib.util.android.IOIOActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;

/**
 * This is the main activity of the HelloIOIO example application.
 * 
 * It displays a toggle button on the screen, which enables control of the
 * on-board LED. This example shows a very simple usage of the IOIO, by using
 * the {@link IOIOActivity} class. For a more advanced use case, see the
 * HelloIOIOPower example.
 */
public class MainActivity extends IOIOActivity {
	private ToggleButton togStart;
	private SeekBar seekPulse;
	private TextView title_;
	private ToggleButton togChA;
	private ToggleButton togChB;
	boolean chAState = false;
	boolean chBState = false;
	boolean pulseState = false;
	volatile String title = "";
	/**
	 * Called when the activity is first created. Here we normally initialize
	 * our GUI.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		title_ = (TextView)findViewById(R.id.title);
		togStart = (ToggleButton)findViewById(R.id.togServo);
		togChA = (ToggleButton)findViewById(R.id.togChA);
		togChB = (ToggleButton)findViewById(R.id.togChB);
		pulseStepTime = (pulseTimeMax - pulseTimeMin)/100;
		seekPulse = (SeekBar)findViewById(R.id.seekPulse);
		seekPulse.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) { }
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) { }
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				pulseTime = pulseTimeMin + pulseStepTime * progress;
			}
		});
		Thread titleUpdater = new Thread(new Runnable() {
			@Override
			public void run() {
				while(true){
					try {
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								title_.setText(title);
								togChA.setChecked(chAState);
								togChB.setChecked(chBState);
								togStart.setChecked(enSpin);
							}
						});
						Thread.sleep(100);
					} catch (Exception e) {
						title = e.getMessage();
					}
				}
			}
		});
		titleUpdater.start();
		
	}

	public void btnLessPulse_onClick(View v){
		pulseTime -= pulseStepTime;
		if(pulseTime < pulseTimeMin) pulseTime = pulseTimeMin;
		seekPulse.setProgress((pulseTime - pulseTimeMin) / pulseStepTime);
	}
	
	public void btnMorePulse_onClick(View v){
		pulseTime += pulseStepTime;
		if(pulseTime > pulseTimeMax) pulseTime = pulseTimeMax;
		seekPulse.setProgress((pulseTime - pulseTimeMin) / pulseStepTime);
	}
	
	public void btnResetCount_onClick(View v){
		changes = 0;
	}
	
	int pulseTime = 1300;
	int pulseTimeMin = 1150;
	int pulseTimeMax = 1450;
	int pulseStepTime = 5;
	boolean lastChA = false;
	boolean lastChB = false;
	volatile boolean enSpin = false;
	int changes = 0;
	class Looper extends BaseIOIOLooper {
		/** The on-board LED. */
		private DigitalOutput led_;
		private PwmOutput io_01;
		private DigitalInput io_02;
		private DigitalInput io_03;
		/**
		 * Called every time a connection with IOIO has been established.
		 * Typically used to open pins.
		 * 
		 * @throws ConnectionLostException
		 *             When IOIO connection is lost.
		 * 
		 * @see ioio.lib.util.AbstractIOIOActivity.IOIOThread#setup()
		 */
		@Override
		protected void setup() throws ConnectionLostException {
			title_.setText(title_.getText()+"S");
			led_ = ioio_.openDigitalOutput(0, true);
			io_01 = ioio_.openPwmOutput(1, 400);
			io_02 = ioio_.openDigitalInput(2);
			io_03 = ioio_.openDigitalInput(3);
		}

		/**
		 * Called repetitively while the IOIO is connected.
		 * 
		 * @throws ConnectionLostException
		 *             When IOIO connection is lost.
		 * 
		 * @see ioio.lib.util.AbstractIOIOActivity.IOIOThread#loop()
		 */
		
		boolean ledState = true;
		long cycle = 0;
		@Override
		public void loop() {
			cycle++;
			if(changes >= 64){
				enSpin = false;
			}
			try {
				chAState = io_02.read();
				chBState = io_03.read();
				if(lastChA != chAState) changes++;
				if(lastChB != chBState) changes++;
				lastChA = chAState;
				lastChB = chBState;
				if(enSpin){
					io_01.setPulseWidth(pulseTime);
//					io_02.setPulseWidth(pulseTime);
//					io_03.setDutyCycle(0.5f);
				}else {
					io_01.setDutyCycle(0.0f);
//					io_02.setDutyCycle(0.0f);
				}
				enSpin = togStart.isChecked();
				title = "Pulse Time: " + pulseTime / 1000.0 + " ms\n";
				title += "Pulse Step Size: " + pulseStepTime + " uSec\n";
				title += "Changes: " + changes + "\n";
			} catch (Exception e) {
				title = cycle + "-3: " + e.getMessage() + "\n";
				StackTraceElement[] trace = e.getStackTrace();
				for(int i=0;i<trace.length;i++){
					title += trace[i].toString();
				}
				
				try {
					Thread.sleep(2000);
				}catch (Exception ee) {}
			}
		}
	}

	/**
	 * A method to create our IOIO thread.
	 * 
	 * @see ioio.lib.util.AbstractIOIOActivity#createIOIOThread()
	 */
	@Override
	protected IOIOLooper createIOIOLooper() {
		return new Looper();
	}
}